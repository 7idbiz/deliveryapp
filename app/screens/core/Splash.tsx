import React from 'react';
import { Text, View } from 'react-native';

const component = (props: any) => (
  <View style={{ backgroundColor: '#041B47', height:'100%', justifyContent: 'center', alignItems: 'center', }}>
    <Text style={{ color:'#FFFFFF'}}>Loading...</Text>
  </View>
);

export default component;