import React from 'react';
import { Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles from '../../styles';
import {
  ScreenHeader,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const { t } = useTranslation();

  return (
    <ScreenView style={[ styles.publicAreaView ]}>
      <ScreenHeader leftIcon="back" title="Fonts" rightIcon="" />
  
      <View style={{backgroundColor:'#eeeeee', flex:1, width:'100%'}}>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Ultralight' }}>Qwertyuiop Asdfghjkl Zxcvbnm 1</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Thin' }}>Qwertyuiop Asdfghjkl Zxcvbnm 2</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Light' }}>Qwertyuiop Asdfghjkl Zxcvbnm 3</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Regular' }}>Qwertyuiop Asdfghjkl Zxcvbnm 4</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Medium' }}>Qwertyuiop Asdfghjkl Zxcvbnm 5</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Semibold' }}>Qwertyuiop Asdfghjkl Zxcvbnm 6</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Bold' }}>Qwertyuiop Asdfghjkl Zxcvbnm 7</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Heavy' }}>Qwertyuiop Asdfghjkl Zxcvbnm 8</Text>
        <Text style={{ fontSize:17, fontFamily:'SFDisplay-Black' }}>Qwertyuiop Asdfghjkl Zxcvbnm 9</Text>
    
        <Text style={{ fontSize:17, fontFamily:'' }}> </Text>
    
        <Text style={{ fontSize:17, fontFamily:'SFText-Light' }}>Qwertyuiop Asdfghjkl Zxcvbnm 1</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-LightItalic' }}>Qwertyuiop Asdfghjkl Zxcvbnm 2</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-Regular' }}>Qwertyuiop Asdfghjkl Zxcvbnm 3</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-RegularItalic' }}>Qwertyuiop Asdfghjkl Zxcvbnm 4</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-Medium' }}>Qwertyuiop Asdfghjkl Zxcvbnm 5</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-MediumItalic' }}>Qwertyuiop Asdfghjkl Zxcvbnm 6</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-Semibold' }}>Qwertyuiop Asdfghjkl Zxcvbnm 7</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-SemiboldItalic' }}>Qwertyuiop Asdfghjkl Zxcvbnm 8</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-Bold' }}>Qwertyuiop Asdfghjkl Zxcvbnm 9</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-BoldItalic' }}>Qwertyuiop Asdfghjkl Zxcvbnm 10</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-Heavy' }}>Qwertyuiop Asdfghjkl Zxcvbnm 11</Text>
        <Text style={{ fontSize:17, fontFamily:'SFText-HeavyItalic' }}>Qwertyuiop Asdfghjkl Zxcvbnm 12</Text>
      </View>

    </ScreenView>
  );

};

export default component;