import React from 'react';
import { View, ScrollView, } from 'react-native';

import {
  AddPhoto,
  Arrow,
  Back,
  BellTapped,
  BiillsIcon,
  BiillsTapped,
  Calendar,
  CalendarIcon,
  Car,
  Clock,
  Cross,
  Eye,
  HomeActive,
  IcCheck,
  Invoice,
  Logout,
  Managers,
  NounAttach,
  NounCall,
  Orders,
  OrdersTapped,
  PhoneCall,
  PhoneIcon,
  Plus,
  ProfileIcon,
  ProfileTaped,
  Route,
  RouteActive,
  Search,
  Security,
  Shape,
  Support,
  Tracking,
  TrackingIcon,
  Truck,
  TruckIcon,
  TruckTaped,
  User,
  Verified,
} from '../../components/IconSvg';

const component = (props: any) => (
  <ScrollView style={{ backgroundColor:'#990000', padding:20 }}>
    <View style={{ flexDirection: 'row' }}>
      <AddPhoto style={{width:22, height:22, margin:5 }} />
      <Arrow style={{width:22, height:22, margin:5 }} />
      <Back style={{width:22, height:22, margin:5 }} />
      <BellTapped style={{width:22, height:22, margin:5 }} />
      <BiillsIcon style={{width:22, height:22, margin:5 }} />
      <BiillsTapped style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <Calendar style={{width:22, height:22, margin:5 }} />
      <CalendarIcon style={{width:22, height:22, margin:5 }} />
      <Car style={{width:22, height:22, margin:5 }} />
      <Clock style={{width:22, height:22, margin:5 }} />
      <Cross style={{width:22, height:22, margin:5 }} />
      <Eye style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <HomeActive style={{width:22, height:22, margin:5 }} />
      <IcCheck style={{width:22, height:22, margin:5 }} />
      <Invoice style={{width:22, height:22, margin:5 }} />
      <Logout style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <Managers style={{width:22, height:22, margin:5 }} />
      <NounAttach style={{width:22, height:22, margin:5 }} />
      <NounCall style={{width:22, height:22, margin:5 }} />
      <Orders style={{width:22, height:22, margin:5 }} />
      <OrdersTapped style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <PhoneCall style={{width:22, height:22, margin:5 }} />
      <PhoneIcon style={{width:22, height:22, margin:5 }} />
      <Plus style={{width:22, height:22, margin:5 }} />
      <ProfileIcon style={{width:22, height:22, margin:5 }} />
      <ProfileTaped style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <Search style={{width:22, height:22, margin:5 }} />
      <Security style={{width:22, height:22, margin:5 }} />
      {/* <Shape style={{width:22, height:22, margin:5 }} /> */}
      <Support style={{width:22, height:22, margin:5 }} />
      <Tracking style={{width:22, height:22, margin:5 }} />
      <TrackingIcon style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <Truck style={{width:22, height:22, margin:5 }} />
      <TruckIcon style={{width:22, height:22, margin:5 }} />
      <TruckTaped style={{width:22, height:22, margin:5 }} />
      <User style={{width:22, height:22, margin:5 }} />
      <Verified style={{width:22, height:22, margin:5 }} />
    </View><View style={{ flexDirection: 'row' }}>
      <Route style={{width:22, height:22, margin:5 }} />
      <RouteActive style={{width:22, height:22, margin:5 }} />
    </View>

  </ScrollView>
);

export default component;