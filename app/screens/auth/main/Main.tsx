import React, { useState } from 'react';
import { Text, View, TouchableOpacity, Platform } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles, { COLORS } from '../../../styles';

import {
  OrderPanel,
  MapView,
  ScreenHeader,
  ScreenView,
} from '../../../components';
import {
  Route,
} from '../../../components/IconSvg';

const component = (props: any) => {
  const { t } = useTranslation();
  const [visibleFlag, setVisibleFlag] = useState<boolean>(false);

  const startOrder = () => {
    setVisibleFlag(!visibleFlag);
  }

  const getMapView = () => {
    if (Platform.OS === "web") {
      return (
        <View style={[ styles.mapViewComponent ]}>
          <Text>MapView</Text>
        </View>
      )  
    }

    return <MapView />;
  }

  return (
    <ScreenView screenPadding="none">
      
      <View style={[ styles.mapViewHolder ]}>
        { getMapView() }
        
        <View style={{backgroundColor:'rgba(0,0,0,0.2)', width:'100%'}}>
          <ScreenHeader leftIcon="" title={ t('title_main_screen') } rightIcon="" />
        </View>
        
        <TouchableOpacity
          style={[ styles.buttonStartOrder ]}
          onPress={startOrder}>
          <Route style={[ styles.buttonStartOrderIcon ]} />
        </TouchableOpacity>

      </View>

      <OrderPanel visibleFlag={visibleFlag} setVisibleFlag={setVisibleFlag} /> 

    </ScreenView>
  );
};

export default component;