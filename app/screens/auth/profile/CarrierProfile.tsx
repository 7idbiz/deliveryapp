import React from 'react';
import { Text, ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import styles from '../../../styles';
import {
  ScreenHeader,
  ScreenView,
} from '../../../components';

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  return (
    <ScreenView style={[ styles.publicAreaView ]}>
      <ScreenHeader leftIcon="back" title={ t('title_profile_screen') } rightIcon="" />
      <Text style={[ styles.colorWhite ]}>Carrier</Text>
    </ScreenView>
  );
};

export default component;