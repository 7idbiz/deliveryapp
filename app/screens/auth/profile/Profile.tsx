import React, { useState } from 'react';
import { Text, ScrollView, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import {
  authFlagState,
} from '../../../core/userReducer';

import styles from '../../../styles';
import {
  InputMobile,
  InputText,
  ScreenHeader,
  ScreenView,
} from '../../../components';
import {
  Verified,
  Security,
  Logout,
} from '../../../components/IconSvg';

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [nameInput, setNameInput] = useState<string>('Vikes Kolomiets');
  const [nameError, setNameError] = useState<boolean>(false);

  const [mobileInput, setMobileInput] = useState<string>('123456789');
  const [mobileError, setMobileError] = useState<boolean>(false);

  const signOutHandler = ()=>{
    dispatch( authFlagState(false) );
  };

  const changePassword = ()=>{
    navigation.navigate('ProfilePasswordScreen');
  }

  // const btnResetPasswordHandler = () => {
  //   const flag1 = (mobileInput.length < 5);

  //   setMobileError( flag1 );

  //   if (!flag1) {
  //     navigation.navigate('profileTabScreen');
  //   }
  // };

  return (
    <ScreenView style={[ styles.publicAreaView ]}>
      <ScreenHeader leftIcon="" title={ t('title_profile_screen') } rightIcon="" />
      
      <Text style={[ styles.formGreyLabel ]}>{ t('title_name') }</Text>
      <InputText 
        value={nameInput} onChange={setNameInput} error={nameError}
        placeholder={ t('placeholder_name') } theme="dark"/>

      <Text style={[ styles.formGreyLabel ]}>{ t('title_phone_number') }</Text>
      <InputMobile 
        value={mobileInput} onChange={setMobileInput} error={mobileError}
        placeholder={ t('enter_your_phone_number') } theme="dark" rightIcon="edit" />

      <TouchableOpacity style={[ styles.rowContainer, {marginTop:35, marginBottom:5, alignItems:'center', } ]}>
        <Verified style={{width:22, height:22, margin:5, marginRight:12 }} />
        <Text style={[ styles.textGrey ]}>{ t('request_trusted_account') }</Text>
      </TouchableOpacity>
      
      <TouchableOpacity 
        onPress={changePassword}
        style={[ styles.rowContainer, {marginTop:5, marginBottom:5, alignItems:'center', } ]}>
        <Security style={{width:22, height:22, margin:5, marginRight:12 }} />
        <Text style={[ styles.textGrey ]}>{ t('change_password') }</Text>
      </TouchableOpacity>

      <TouchableOpacity 
        onPress={signOutHandler}
        style={[ styles.rowContainer, {marginTop:5, marginBottom:25, alignItems:'center', } ]}>
        <Logout style={{width:22, height:22, margin:5, marginLeft:8, marginRight:10 }} />
        <Text style={[ styles.textGrey ]}>{ t('sign_out') }</Text>
      </TouchableOpacity>

      <TouchableOpacity 
        onPress={()=>{
          navigation.navigate('IconsScreen');
        }}
        style={[ styles.rowContainer, {marginTop:5, marginBottom:5, alignItems:'center', } ]}>
        <Text style={[ styles.textGrey ]}>Icons</Text>
      </TouchableOpacity>
      
      <TouchableOpacity 
        onPress={()=>{
          navigation.navigate('FontsScreen');
        }}
        style={[ styles.rowContainer, {marginTop:5, marginBottom:5, alignItems:'center', } ]}>
        <Text style={[ styles.textGrey ]}>Fonts</Text>
      </TouchableOpacity>

    </ScreenView>
  );
};

export default component;