import React, { useState } from 'react';
import { Text, ScrollView, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import styles, { COLORS } from '../../../styles';
import {
  ButtonFilled,
  ButtonOpacity,
  ScreenHeader,
  ScreenView,
  InputPassword,
} from '../../../components';

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const [passOldInput, setPassOldInput] = useState<string>('');
  const [passOldError, setPassOldError] = useState<boolean>(false);
  
  const [passInput, setPassInput] = useState<string>('');
  const [passError, setPassError] = useState<boolean>(false);
  const [pass2Input, setPass2Input] = useState<string>('');
  const [pass2Error, setPass2Error] = useState<boolean>(false);

  const btnForgotHandler = () => {
    navigation.navigate('ProfileForgotScreen');
  };

  const btnConfirmHandler = () => {
    const flag1 = (passOldInput.length < 5);
    const flag2 = (passInput.length < 5);
    const flag3 = (pass2Input.length < 5);

    setPassOldError( flag1 );
    setPassError( flag2 );
    setPass2Error( flag3 );
    if (passInput != pass2Input) {
      setPassError( true );
      setPass2Error( true );
    }

    if (!flag1 && !flag2 && (passInput == pass2Input)) {
      navigation.navigate('profileTabScreen');
    }
  };

  return (
    <ScreenView style={[ styles.publicAreaView ]}>
      <ScreenHeader leftIcon="back" title={ t('change_password') } rightIcon="" />

      <Text style={[ styles.formGreyLabel ]}>{ t('title_current_password') }</Text>
      <InputPassword 
        value={passOldInput} onChange={setPassOldInput} error={passOldError} theme="dark" />      
      
      <Text style={[ styles.formGreyLabel ]}>{ t('title_new_password') }</Text>
      <InputPassword 
        value={passInput} onChange={setPassInput} error={passError} theme="dark" />

      <Text style={[ styles.formGreyLabel ]}>{ t('title_repeat_new_password') }</Text>
      <InputPassword 
        value={pass2Input} onChange={setPass2Input} error={pass2Error} theme="dark" />

      <ButtonFilled title={ t('title_change_password' )} onPress={btnConfirmHandler} />
      <ButtonOpacity title={ t('forgot_password') } onPress={btnForgotHandler} style={[ {marginTop:20} ]} />
      
    </ScreenView>
  );
};

export default component;