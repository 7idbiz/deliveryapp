import React, { useState } from 'react';
import { Text, ScrollView, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import {
  authFlagState,
} from '../../../core/userReducer';

import styles from '../../../styles';
import {
  InputMobile,
  ButtonFilled,
  ScreenHeader,
  ScreenView,
} from '../../../components';

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const [mobileInput, setMobileInput] = useState<string>('123456789');
  const [mobileError, setMobileError] = useState<boolean>(false);
  
  const btnResetPasswordHandler = () => {
    const flag1 = (mobileInput.length < 5);

    setMobileError( flag1 );

    if (!flag1) {
      navigation.navigate('profileTabScreen');
    }
  };

  return (
    <ScreenView style={[ styles.publicAreaView ]}>
      <ScreenHeader leftIcon="back" title={ t('forgot_password') } rightIcon="" />
      <Text style={[ styles.textDescription ]}>{ t('text_forgot_your_password') }</Text>
        
      <InputMobile 
        value={mobileInput} onChange={setMobileInput} error={mobileError}
        placeholder={ t('enter_your_phone_number') } theme="dark"/>

      <ButtonFilled title={ t('title_reset_password') } onPress={btnResetPasswordHandler} />
      
    </ScreenView>
  );
};

export default component;