import React from 'react';
import { Text, FlatList, TouchableOpacity, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import styles, { COLORS } from '../../../styles';
import {
  ScreenHeader,
  ScreenView,
} from '../../../components';
import {
  Arrow,
} from '../../../components/IconSvg'

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const goEventDetails = (keys: string)=>{
    navigation.navigate('OrderDetailsScreen', {
      keys: keys
    });
  }

  return (
    <ScreenView>
      <ScreenHeader leftIcon="" title={ t('title_orders_screen') } rightIcon="order" />

      <FlatList
        data={[
          {id:'0010', keys: 'Your  order Hamburg - Berlin was successfully complited'},
          {id:'0011', keys: 'Third car of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0012', keys: 'Two cars of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0013', keys: 'Cars from your order Hamburg - Berlin are loaded'},
          {id:'0014', keys: 'Your  order Hamburg - Berlin has been confirmed by the driver'},
          {id:'0015', keys: 'Third car of your order Hamburg - Baden-Baden is 15 km from the destination point'},
          {id:'0016', keys: 'Your  order Hamburg - Berlin was successfully complited'},
          {id:'0017', keys: 'Third car of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0018', keys: 'Two cars of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0019', keys: 'Cars from your order Hamburg - Berlin are loaded'},
          {id:'0020', keys: 'Your  order Hamburg - Berlin has been confirmed by the driver'},
          {id:'0021', keys: 'Third car of your order Hamburg - Baden-Baden is 15 km from the destination point'},
          {id:'0022', keys: 'Your  order Hamburg - Berlin was successfully complited'},
          {id:'0023', keys: 'Third car of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0024', keys: 'Two cars of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0025', keys: 'Cars from your order Hamburg - Berlin are loaded'},
          {id:'0026', keys: 'Your  order Hamburg - Berlin has been confirmed by the driver'},
          {id:'0027', keys: 'Third car of your order Hamburg - Baden-Baden is 15 km from the destination point'},
        ]}
        renderItem={({item}) => <TouchableOpacity 
          onPress={()=>{
            goEventDetails(item.keys);
          }}
          key={item.id} 
          style={[ styles.eventShortDetails ]}>
          <View style={[ styles.rowContainer ]}>
            <Text style={[ styles.cityYellow ]}>Hamburg</Text>
            <Arrow style={{width:14, height:8, margin:5, marginLeft:15, marginRight:15 }} />
            <Text style={[ styles.cityYellow ]}>Berlin</Text>
          </View>
          <Text style={[ styles.colorWhite, { marginTop:10 } ]}>{item.keys}</Text>
        </TouchableOpacity> }
      />
    </ScreenView>
  );
};

export default component;