import React from 'react';
import { Text, ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles from '../../../styles';
import {
  ScreenHeader,
  ScreenView,
} from '../../../components';

const component = (props: any) => {
  const { t } = useTranslation();

  return (
    <ScreenView>
      <ScreenHeader leftIcon="back" title={ t('title_orders_screen') } rightIcon="" />
      <Text style={[ styles.colorWhite ]}>{ props.route.params.keys }</Text>
    </ScreenView>
  );
};

export default component;