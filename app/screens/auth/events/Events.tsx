import React from 'react';
import { Text, FlatList, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import styles from '../../../styles';
import {
  ScreenHeader,
  ScreenView,
} from '../../../components';

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const goEventDetails = (keys: string)=>{
    navigation.navigate('EventDetailsScreen', {
      keys: keys
    });
  }

  return (
    <ScreenView>
      <ScreenHeader leftIcon="" title={ t('title_events_screen') } rightIcon="search" />
      
      <FlatList
        data={[
          {id:'0010', keys: 'Your  order Hamburg - Berlin was successfully complited'},
          {id:'0011', keys: 'Third car of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0012', keys: 'Two cars of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0013', keys: 'Cars from your order Hamburg - Berlin are loaded'},
          {id:'0014', keys: 'Your  order Hamburg - Berlin has been confirmed by the driver'},
          {id:'0015', keys: 'Third car of your order Hamburg - Baden-Baden is 15 km from the destination point'},
          {id:'0016', keys: 'Your  order Hamburg - Berlin was successfully complited'},
          {id:'0017', keys: 'Third car of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0018', keys: 'Two cars of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0019', keys: 'Cars from your order Hamburg - Berlin are loaded'},
          {id:'0020', keys: 'Your  order Hamburg - Berlin has been confirmed by the driver'},
          {id:'0021', keys: 'Third car of your order Hamburg - Baden-Baden is 15 km from the destination point'},
          {id:'0022', keys: 'Your  order Hamburg - Berlin was successfully complited'},
          {id:'0023', keys: 'Third car of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0024', keys: 'Two cars of your order Hamburg - Berlin is 15 km from the destination point'},
          {id:'0025', keys: 'Cars from your order Hamburg - Berlin are loaded'},
          {id:'0026', keys: 'Your  order Hamburg - Berlin has been confirmed by the driver'},
          {id:'0027', keys: 'Third car of your order Hamburg - Baden-Baden is 15 km from the destination point'},
        ]}
        renderItem={({item}) => <TouchableOpacity 
          onPress={()=>{
            goEventDetails(item.keys);
          }}
          key={item.id} 
          style={[ styles.eventShortDetails ]}>
          <Text style={[ styles.colorWhite ]}>{item.keys}</Text>
        </TouchableOpacity> }
      />
      
    </ScreenView>
  );
};

export default component;