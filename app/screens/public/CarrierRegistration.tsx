import React, { useEffect, useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { RootState } from '../../core/store';
import { fetchCreateUser } from '../../core/userReducer';
import styles, { COLORS } from '../../styles';
import {
  ButtonOutline,
  ButtonFilled,
  InputCheck,
  InputText,
  InputPassword,
  PublicView,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);

  const [mobileInput, setMobileInput] = useState<string>('User');
  const [mobileError, setMobileError] = useState<boolean>(false);
  const [passInput, setPassInput] = useState<string>('password');
  const [passError, setPassError] = useState<boolean>(false);
  const [pass2Input, setPass2Input] = useState<string>('password');
  const [pass2Error, setPass2Error] = useState<boolean>(false);

  useEffect(()=>{
    // dispatch( fetchCreateUser() );
    // setTimeout( ()=> {
      // navigation.navigate('IconsScreen');
    // }, 5000)
  }, []);

  const btnConfirmHandler = () => {
    const flag1 = (mobileInput.length < 3);
    const flag2 = (passInput.length < 5);
    const flag3 = (pass2Input.length < 5);

    setMobileError( flag1 );
    setPassError( flag2 );
    setPass2Error( flag3 );
    if (passInput != pass2Input) {
      setPassError( true );
      setPass2Error( true );
    }

    if (!flag1 && !flag2 && (passInput == pass2Input)) {
      navigation.navigate('LoaderFullScreen');
    }
  };

  const btnLoggingHandler = () => {
    navigation.navigate('SignInScreen');
  };

  return (
    <ScreenView keyboardView={true} backgroundColor={COLORS.darkblue}>
      <PublicView>
        
        <Text style={[ styles.screenTitle ]}>{ t('title_registration') }</Text>
        
        <InputCheck leftTitle="Carrier" rightTitle="Partner Carrier" />
        <InputText 
          value={mobileInput} onChange={setMobileInput} error={mobileError}
          placeholder={ t('placeholder_name') } />
        <InputPassword 
          value={passInput} onChange={setPassInput} error={passError}
          placeholder={ t('enter_your_password') } />
        <InputPassword 
          value={pass2Input} onChange={setPass2Input} error={pass2Error}
          placeholder={ t('repeat_password') } />

        <ButtonFilled title={ t('title_confirm_registration') } onPress={btnConfirmHandler} />
        <ButtonOutline title={ t('title_sign_in') } onPress={btnLoggingHandler}/>
        <View style={{marginBottom:100}}></View>
      </PublicView>
    </ScreenView>
  );
};

export default component;