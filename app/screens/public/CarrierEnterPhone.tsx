import React, { useEffect, useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { RootState } from '../../core/store';
import { fetchCreateUser } from '../../core/userReducer';
import styles, { COLORS } from '../../styles';
import {
  ButtonFilled,
  ButtonOpacity,
  ButtonOutline,
  InputMobile,
  PublicView,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);

  const [mobileInput, setMobileInput] = useState<string>('1234567');
  const [mobileError, setMobileError] = useState<boolean>(false);

  useEffect(()=>{
    // dispatch( fetchCreateUser() );
    // setTimeout( ()=> {
      // navigation.navigate('IconsScreen');
    // }, 5000)
  }, []);

  const btnCreateHandler = () => {
    const flag1 = (mobileInput.length < 5);
    setMobileError( flag1 );

    if (!flag1) {
      navigation.navigate('CarrierRegistrationScreen');
    }
  };

  const btnForgotHandler = () => {
    navigation.navigate('ForgotPasswordScreen');
  };

  const btnSignHandler = () => {
    navigation.navigate('SignInScreen');
  };

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <PublicView>
        <Text style={[ styles.screenTitle ]}>{ t('title_carrier_phone') }</Text>
        
        <InputMobile 
          value={mobileInput} onChange={setMobileInput} error={mobileError}
          placeholder={ t('enter_your_phone_number') } />

        <ButtonFilled title={ t('title_create_account') } onPress={btnCreateHandler} />
        <ButtonOpacity title={ t('forgot_password') } onPress={btnForgotHandler} />
        <ButtonOutline title={ t('title_sign_in') } onPress={btnSignHandler} />
        
      </PublicView>
    </ScreenView>
  );
};

export default component;