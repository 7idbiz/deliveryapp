import React, { useEffect, useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { RootState } from '../../core/store';
import { fetchCreateUser } from '../../core/userReducer';
import styles, { COLORS } from '../../styles';
import {
  ButtonFilled,
  ButtonOpacity,
  InputMobile,
  InputPassword,
  PublicView,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);

  const [mobileInput, setMobileInput] = useState<string>('123456789');
  const [mobileError, setMobileError] = useState<boolean>(false);

  useEffect(()=>{
    // dispatch( fetchCreateUser() );
    // setTimeout( ()=> {
      // navigation.navigate('IconsScreen');
    // }, 5000)
  }, []);

  const btnResetPasswordHandler = () => {
    const flag1 = (mobileInput.length < 5);

    setMobileError( flag1 );

    if (!flag1) {
      navigation.navigate('EnterPasswordScreen');
    }
  };

  const btnLoggingAgainHandler = () => {
    navigation.navigate('SignInScreen');
  };

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <PublicView>
        <Text style={[ styles.screenTitle ]}>{ t('title_forgot_your_password') }</Text>
        <Text style={[ styles.textDescription ]}>{ t('text_forgot_your_password') }</Text>
        
        <InputMobile 
          value={mobileInput} onChange={setMobileInput} error={mobileError}
          placeholder={ t('enter_your_phone_number') } />

        <ButtonFilled title={ t('title_reset_password') } onPress={btnResetPasswordHandler} />
        <ButtonOpacity title={ t('try_logging_in_again') } onPress={btnLoggingAgainHandler} />
        
      </PublicView>
    </ScreenView>
  );
};

export default component;