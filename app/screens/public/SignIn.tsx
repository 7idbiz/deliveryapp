import React, { useEffect, useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { RootState } from '../../core/store';
import { fetchCreateUser } from '../../core/userReducer';
import styles, { COLORS } from '../../styles';
import {
  ButtonFilled,
  ButtonOutline,
  ButtonOpacity,
  InputMobile,
  InputPassword,
  PublicView,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);
  
  const [mobileInput, setMobileInput] = useState<string>('123456789');
  const [mobileError, setMobileError] = useState<boolean>(false);
  const [passInput, setPassInput] = useState<string>('password');
  const [passError, setPassError] = useState<boolean>(false);

  useEffect(()=>{
    //navigation.navigate('LoaderFullScreen');
  }, [props]);

  const btnSignInHandler = () => {
    const flag1 = (mobileInput.length < 5);
    const flag2 = (passInput.length < 5);

    setMobileError( flag1 );
    setPassError( flag2 );

    if (!flag1 && !flag2) {
      navigation.navigate('LoaderFullScreen');
      // dispatch( fetchCreateUser() );
    }
  };

  const btnForgotPasswordHandler = () => {
    navigation.navigate('ForgotPasswordScreen');
  };

  const btnRegistrHandler = () => {
    navigation.navigate('RegistrationScreen');
  };

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <PublicView>
        <Text style={[ styles.screenTitle ]}>{ t('title_sign_in') }</Text>
        
        <InputMobile 
          value={mobileInput} onChange={setMobileInput} error={mobileError}
          placeholder={ t('enter_your_phone_number') } />
        <InputPassword 
          value={passInput} onChange={setPassInput} error={passError}
          placeholder={ t('enter_your_password') } />

        <ButtonFilled title={ t('title_sign_in') } onPress={btnSignInHandler} />
        <ButtonOpacity title={ t('forgot_password') } onPress={btnForgotPasswordHandler} />
        <ButtonOutline title={ t('title_registration') } onPress={btnRegistrHandler} />
      </PublicView>
    </ScreenView>
  );
};

export default component;