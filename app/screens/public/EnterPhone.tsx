import React, { useEffect, } from 'react'; 
import { View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { RootState } from '../../core/store';
import styles, { COLORS } from '../../styles';
import {
  ButtonOutline,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);

  useEffect(()=>{
  }, []);

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <View style={[ styles.centerContainer, {flex:1} ]}>
        <View>
          {/* LOGO */}
          <ButtonOutline title="Client" onPress={()=>{
            navigation.navigate('SignInScreen');
          }} />
          <ButtonOutline title="Carrier" onPress={()=>{
            navigation.navigate('CarrierEnterPhoneScreen');
          }} />
        </View>
      </View>
    </ScreenView>
  );
};

export default component;