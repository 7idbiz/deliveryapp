import React from 'react';
import { Text, ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles, { COLORS } from '../../styles';
import {
  ScreenHeader,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const { t } = useTranslation();

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <ScreenHeader leftIcon="back" title={ t('terms_of_service_btn') } rightIcon="" />
    </ScreenView>
  );
};

export default component;