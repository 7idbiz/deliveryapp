import React, { useEffect, } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { RootState } from '../../core/store';
import { fetchCreateUser } from '../../core/userReducer';
import styles, { COLORS } from '../../styles';
import {
  ButtonFilled,
  ButtonOpacity,
  InputNumb,
  PublicView,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);

  useEffect(()=>{
    // dispatch( fetchCreateUser() );
    // setTimeout( ()=> {
      // navigation.navigate('IconsScreen');
    // }, 5000)
  }, []);

  const btnConfirmHandler = () => {
    navigation.navigate('CarrierRegistrationScreen');
  };

  const btnResendHandler = () => {
    // navigation.navigate('SignInScreen');
  };

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <PublicView>
        <Text style={[ styles.screenTitle ]}>{ t('title_confirmation') }</Text>
        <Text style={[ styles.textDescription ]}>{ t('text_enter_digit_code') }</Text>
        
        <View style={[ styles.rowContainer ]}>
          <InputNumb value="1" />
          <InputNumb value="2" />
          <InputNumb value="3" />
          <InputNumb value="4" />
        </View>
        
        <ButtonFilled title={ t('title_confirm') } onPress={btnConfirmHandler} />
        <ButtonOpacity title={ t('resend_code') } onPress={btnResendHandler} />
        
      </PublicView>
    </ScreenView>
  );
};

export default component;