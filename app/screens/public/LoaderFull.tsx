import React, { useEffect, } from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';

import {
  authFlagState,
} from '../../core/userReducer';
import styles, { COLORS } from '../../styles';
import {
  ScreenView,
} from '../../components';

const component = (props: any) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(()=>{
    setTimeout( ()=> {
      dispatch( authFlagState(true) );
    }, 2000)
  }, []);

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <View style={[ styles.centerContainer, {flex:1} ]}>
        <View>
          {/* LOGO */}
          <ActivityIndicator size="large" color={COLORS.white} style={{margin:50}} />
        </View>
      </View>
    </ScreenView>
  );
};

export default component;