import React, { useState } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import styles, { COLORS } from '../../styles';
import {
  ButtonFilled,
  InputPassword,
  PublicView,
  ScreenView,
} from '../../components'

const component = (props: any) => {
  const navigation = useNavigation();
  const { t } = useTranslation();

  const [passInput, setPassInput] = useState<string>('password');
  const [passError, setPassError] = useState<boolean>(false);
  const [pass2Input, setPass2Input] = useState<string>('password');
  const [pass2Error, setPass2Error] = useState<boolean>(false);

  const btnResetPasswordHandler = () => {
    const flag1 = (passInput.length < 5);
    const flag2 = (pass2Input.length < 5);

    setPassError( flag1 );
    setPass2Error( flag2 );
    if (passInput != pass2Input) {
      setPassError( true );
      setPass2Error( true );
    }

    if (!flag1 && !flag2 && (passInput == pass2Input)) {
      navigation.navigate('LoaderFullScreen');
    }
  };

  return (
    <ScreenView backgroundColor={COLORS.darkblue}>
      <PublicView>
        <Text style={[ styles.screenTitle ]}>{ t('title_enter_your_password') }</Text>
        
        <InputPassword 
          value={passInput} onChange={setPassInput} error={passError}
          placeholder={ t('enter_your_password') } />
        <InputPassword 
          value={pass2Input} onChange={setPass2Input} error={pass2Error}
          placeholder={ t('repeat_password') } />

        <ButtonFilled title={ t('title_confirm_registration') } onPress={btnResetPasswordHandler} />
      </PublicView>
    </ScreenView>
  );
};

export default component;