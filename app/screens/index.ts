
// *********************************** CORE ************************************
export { default as FontsScreen } from './core/Fonts';
export { default as IconsScreen } from './core/Icons';
export { default as SplashScreen } from './core/Splash';

// ************************************ AUTH ***********************************
export { default as EventsScreen } from './auth/events/Events';
export { default as EventDetailsScreen } from './auth/events/EventDetails';

export { default as MainScreen } from './auth/main/Main';

export { default as OrdersScreen } from './auth/orders/Orders';
export { default as OrderDetailsScreen } from './auth/orders/OrderDetails';

export { default as CarrierProfileScreen } from './auth/profile/CarrierProfile';
export { default as ClientProfileScreen } from './auth/profile/ClientProfile';
export { default as ProfileScreen } from './auth/profile/Profile';
export { default as ProfileForgotScreen } from './auth/profile/ProfileForgot';
export { default as ProfilePasswordScreen } from './auth/profile/ProfilePassword';

// *********************************** PUBLIC **********************************
export { default as CarrierConfirmationScreen } from './public/CarrierConfirmation';
export { default as CarrierEnterPhoneScreen } from './public/CarrierEnterPhone';
export { default as CarrierRegistrationScreen } from './public/CarrierRegistration';

export { default as EnterPasswordScreen } from './public/EnterPassword';
export { default as EnterPhoneScreen } from './public/EnterPhone';
export { default as ForgotPasswordScreen } from './public/ForgotPassword';
export { default as RegistrationScreen } from './public/Registration';
export { default as LoaderFullScreen } from './public/LoaderFull';
export { default as SignInScreen } from './public/SignIn';
export { default as TermsOfServiceScreen } from './public/TermsOfService';


