import 'react-native-url-polyfill/auto';
import 'react-native-gesture-handler';
import React from 'react';
import { View, Text } from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider as ReduxProvider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import store from './core/store';
import './i18n';

import Layouts from './navigation/Layouts';

export default function App() {
  return (
    <SafeAreaProvider>
      <ReduxProvider store={ store }>
        <NavigationContainer>
          <Layouts />
        </NavigationContainer>
      </ReduxProvider>
    </SafeAreaProvider>
  );
};
