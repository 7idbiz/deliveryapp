import { StyleSheet } from 'react-native';
import { 
  COLORS,

  getScreenPadding,
  getTabIconSize,
  getTabPanelHeight,
} from './stylesUtils';

const styles = StyleSheet.create({

  /*******************************************  CONTAINERS  ************************************/
  safeAreaView: {
    height: '100%',
    paddingRight: getScreenPadding(),
    paddingLeft: getScreenPadding(),
  },

  publicAreaView: {
    flex: 1,
  },

  rowContainer: {
    flexDirection: 'row', 
  },

  centerContainer: {
    justifyContent:'center', 
    alignItems:'center',
  },

  headerScreenContainer: { 
    marginBottom:10, 
    marginRight:-24, 
    marginLeft: -24,
    flexDirection: 'row', 
  },
  
  headerScreenIconContainer: {
    flex:1, 
    padding:10, 
    paddingTop:20, 
    minHeight:50,
  },

  footerTermsContainer: {
    minHeight: 30, 
    position: 'absolute', 
    bottom:0, 
    left:0, 
    right:0, 
    flexDirection: 'row', 
    justifyContent: 'center',
  },

  eventShortDetails: {
    margin:7, 
    padding:5, 
    backgroundColor: 'rgba(200, 200, 200, 0.1)',
    borderRadius: 5,
  },

  mapViewHolder: {
    backgroundColor: COLORS.grey, 
    alignItems:'center',
    height: '100%',
  },

  mapViewComponent: {
    position: 'absolute', 
    left: 0, 
    right: 0, 
    top: 0, 
    bottom: 0,
  },

  orderPanelContainer: {
    backgroundColor: 'rgba(0,0,0,.1)',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
  },

  orderFormContainer: {
    backgroundColor: COLORS.black,
    position:'absolute',
    minHeight: 100,
    borderTopStartRadius: 11,
    borderTopEndRadius: 11,
    bottom: 0,
    left: 0,
    right: 0,
  },

  orderPanelSelectCarTypeBg: {
    margin:7,
    flex:1,
    height: 70,
    borderRadius:7,
  },

  orderPanelSelectDay: {
    margin: 7,
    flexDirection: 'row',
    alignItems: 'center',
  },

  orderPanelSelectDayLabel: {
    color: COLORS.white,
    fontSize: 15,
    fontFamily: "SFDisplay-Bold",
    flex:1,
  },

  orderPanelSelectDayButton: {
    width: 170,
    height: 40,
    borderColor: COLORS.white,
    borderWidth: 1,  
    borderRadius: 50, 
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  orderPanelSelectDayButtonLabel: {
    color: COLORS.white,
    fontSize: 13,
    fontFamily: "SFDisplay-Regular",
  },

  orderPanelSelectCountCar: {
    flex:1,
    height:40,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  }, 

  orderPanelSelectCarTypeItem: {
    padding:7,
    flex:1,
    borderRadius:7,
    justifyContent:'flex-end', 
    backgroundColor: 'rgba(0,0,0,0.3)'
  },

  /******************************************  BUTTONS  ***********************************/
  buttonOpacityContainer: {
    padding: 10,
    alignItems: 'center',
  },
  buttonOpacityTitle: {
    opacity: 0.7,
    color: COLORS.white,
    fontFamily: "SFDisplay-Regular",
    fontSize: 16,
    letterSpacing: 0,
    lineHeight: 18,
    textDecorationLine: 'underline',
  },

  buttonOutlineContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: COLORS.yellow,
    borderWidth: 2,  
    borderRadius: 50, 
    height: 50,
    marginTop: 16,
    marginBottom: 6,
  },
  buttonOutlineTitle: {
    color: COLORS.yellow,
    fontFamily: "SFDisplay-Heavy",
    fontSize: 15,
    letterSpacing: 0,
    lineHeight: 18,
  },

  buttonCross: {
    justifyContent:'center', 
    alignItems:'center',
    width:50,
  },

  tabIconStyle: {
    width: getTabIconSize(),
    height: getTabIconSize(),
  },

  tabPanelStyles: {
    borderTopWidth: 0,
    height: getTabPanelHeight(),
  },

  buttonStartOrder: {
    position: 'absolute',
    bottom: 0,
    borderRadius: 100,
    width: 80,
    height: 80,
    margin:0,
    padding:0,
  },

  buttonStartOrderIcon: {
    width: 80,
    height: 80,
  },

  /******************************************  INPUTS  ***********************************/
  inputTextContainer: {
    justifyContent: 'center',
    backgroundColor: COLORS.white,  
    borderColor: COLORS.white,
    borderWidth: 2,
    borderRadius: 50, 
    height: 50,
    marginTop: 16,
    marginBottom: 6,
    paddingLeft: 20,
    paddingRight: 20,
  },
  inputTextContainerDark: {
    justifyContent: 'center',
    backgroundColor: 'rgba(250,250,250,0.1)',  
    borderColor: 'rgba(250,250,250,0)',
    borderWidth: 2,
    borderRadius: 9, 
    height: 50,
    marginTop: 16,
    marginBottom: 6,
    paddingLeft: 20,
    paddingRight: 20,
  },
  inputText: {
    color: COLORS.black,
    fontFamily: "SFDisplay-Medium",
    flex:1,
    fontSize: 15,
    letterSpacing: 0.54,
    lineHeight: 18,
  },
  inputNumbContainer: {
    justifyContent: 'center',
    backgroundColor: COLORS.white,  
    borderRadius: 5, 
    height: 60,
    margin: 5,
    width: 70,
    marginTop: 20,
    marginBottom: 20,
  },
  inputNumb: {
    color: COLORS.black,
    fontFamily: "SFDisplay-Medium",
    textAlign: 'center',
    fontSize: 20,
    letterSpacing: 0.54,
    lineHeight: 18,
    paddingTop: 20,
    paddingBottom: 20,
  },
  inputCheckBtn: {
    backgroundColor: COLORS.yellow,
    margin:2,
    flex:1,
    borderRadius: 50, 
    justifyContent:'center', 
    alignItems:'center',
  },

  /*******************************************  TEXTS  ************************************/
  colorWhite: {
    color: COLORS.white,
  },

  textUnderline: {
    textDecorationLine: 'underline'
  },

  screenTitle: {
    color: COLORS.white,
    fontFamily: "SFDisplay-Black",
    fontSize: 15,
    fontWeight: "900",
    letterSpacing: 1,
    lineHeight: 18,
    textAlign: "center", 
    paddingTop: 20,
  },

  footerTerms: {
    color: COLORS.white,
    opacity: 0.7,
    fontFamily: "SFDisplay-Regular",
    fontSize: 11,
    letterSpacing: 0,
    lineHeight: 19,
  },

  textDescription: {
    opacity: 0.7,
    color: COLORS.white,
    fontFamily: "SFDisplay-Medium",
    fontSize: 15,
    letterSpacing: 0,
    lineHeight: 18,
    paddingTop:10,
  },

  textGrey: {
    opacity: 0.3,
    color: COLORS.white,
    fontFamily: "SFDisplay-Regular",
    fontSize: 16,
    letterSpacing: 0,
    lineHeight: 19,
  },

  cityYellow: {
    color: COLORS.yellow,
    fontFamily: "SFDisplay-Semibold",
    fontSize: 15,
    lineHeight: 18,
  },

  orderPanelSelectCarTypeItemTitle: {
    color: COLORS.white,
    fontFamily: "SFDisplay-Black",
    fontSize: 16,
    letterSpacing: 0,
    lineHeight: 19,
  },

  orderPanelSelectCarTypeItemLabel: {
    color: COLORS.white,
    fontFamily: "SFDisplay-Semibold",
    fontSize: 11,
    letterSpacing: 0,
    lineHeight: 13,
  },

  formGreyLabel: {
    opacity: 0.3,
    color: COLORS.white,
    fontFamily: "SFDisplay-Regular",
    fontSize: 11,
    letterSpacing: 0,
    lineHeight: 15,
    marginBottom: -10,
    marginTop: 20,
  },

});

export default styles;

