import { Dimensions } from 'react-native';

export const COLORS = {
  // base colors
  white: "#FFFFFF",
  grey: "#595F66",
  darkblue: "#041B47",
  black: "#171B26",
  yellow: "#F9CF1C",
};

export const DEVICE = {
  tabletMinWidth: 600,
  tabletMinHeight: 700,
};

export const getScreenPadding = () => {
  if (Dimensions.get('window').width < DEVICE.tabletMinWidth) {
    return 24;
  }
  
  const step = (Dimensions.get('window').width - DEVICE.tabletMinWidth) * 0.5;
  return Math.floor(step);
};

export const getTabIconSize = () => {
  if (Dimensions.get('window').height < DEVICE.tabletMinHeight) {
    return 22;
  }
  
  return 30;
};

export const getTabPanelHeight = () => {
  if (Dimensions.get('window').height < DEVICE.tabletMinHeight) {
    return 50;
  }

  return 70;
};

