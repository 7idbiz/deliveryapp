import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

import * as apiClient from './apiClient';

const namespace = 'userProfile';

export type UserProfileState = {
	loadingFlag: boolean;
	authFlag: boolean;
	phone: string;
}

const initialState: UserProfileState = {
	loadingFlag: false,
	authFlag: false,
	phone: "",
};

export const fetchCreateUser = createAsyncThunk(
	`${namespace}/fetchCreateUser`,
	async () => {
		const response = await apiClient.fetchCreateUser('phone911')
			.then(data => {
				console.log('____reducer then___', data );		
			})
		console.log('____reducer___', response );
		// return true;

		// return {
		// 	flag:true
		// };

		// const response = await fetch(
		// 	url + 'otp?phone=222333', POST
		// );
		// console.log("____axios response____", response );

		// const data = await new Promise((resolve, reject) => setTimeout(()=>{
		// 	// console.log("reducer 222");
		// 	// resolve('wwwww');
		// 	reject('qweqwe')
		// }, 5000));
		// console.log("reducer 333");

		// const { data } = await axios.post('qweqwe')
		// return data;
	}
);

const userProfile = createSlice({
	name: namespace,
	initialState: initialState,
	reducers: {
		loadingFlagState: (state, action) => {
			state.loadingFlag = action.payload;
		},
		authFlagState: (state, action) => {
			state.authFlag = action.payload;
		},
		userPhone: (state, action) => {
			state.phone = action.payload;
		},
		userClean: (state, action) => {
			state.phone = "";
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchCreateUser.pending, (state, action) => {
      console.log("pending",state, action);
    }),
		builder.addCase(fetchCreateUser.fulfilled, (state, action) => {
      console.log("fulfilled",state, action);
    }),
		builder.addCase(fetchCreateUser.rejected, (state, action) => {
      console.log("rejected",state, action);
    })
	}
});

export const { 
	loadingFlagState,
	authFlagState,
	
	userPhone,
	userClean,
} = userProfile.actions;

export default userProfile.reducer;