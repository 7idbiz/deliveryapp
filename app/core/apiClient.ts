type ResponseKind = 'success' | 'failure';

type NetworkResponse<T> = {
  kind: ResponseKind;
  body?: T;
}

const POST = { 
	method: 'POST'
};

export const fetchCreateUser = async (
  phone: string
): Promise<NetworkResponse<ResponseKind>> => {

  const response = await fetch(
    url + `otp?phone=${phone}`, POST
  );

  console.log("____axios response____", response );
  // return true;

  if (response.ok) {
    const json = await response.json();
    return {
      kind: 'success',
      body: json.results,
    }
  }

  return {
    kind: 'failure',
  }
  
};
