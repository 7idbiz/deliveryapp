
export { default as AddPhoto } from './svg/AddPhoto';
export { default as Arrow } from './svg/Arrow';
export { default as BellTapped } from './svg/BellTapped';
export { default as BiillsIcon } from './svg/BiillsIcon';
export { default as BiillsTapped } from './svg/BiillsTapped';

export { default as Calendar } from './svg/Calendar';
export { default as CalendarIcon } from './svg/CalendarIcon';
export { default as Car } from './svg/Car';
export { default as Clock } from './svg/Clock';
export { default as Eye } from './svg/Eye';

export { default as HomeActive } from './svg/HomeActive';
export { default as IcCheck } from './svg/IcCheck';
export { default as Invoice } from './svg/Invoice';

export { default as Logout } from './svg/Logout';
export { default as Managers } from './svg/Managers';
export { default as NounAttach } from './svg/NounAttach';
export { default as NounCall } from './svg/NounCall';

export { default as Orders } from './svg/Orders';
export { default as OrdersTapped } from './svg/OrdersTapped';
export { default as PhoneCall } from './svg/PhoneCall';
export { default as PhoneIcon } from './svg/PhoneIcon';
export { default as Plus } from './svg/Plus';

export { default as ProfileIcon } from './svg/ProfileIcon';
export { default as ProfileTaped } from './svg/ProfileTaped';
export { default as Route } from './svg/Route';
export { default as RouteActive } from './svg/RouteActive';
export { default as Search } from './svg/Search';

export { default as Security } from './svg/Security';
export { default as Shape } from './svg/Shape';
export { default as Support } from './svg/Support';
export { default as Tracking } from './svg/Tracking';
export { default as TrackingIcon } from './svg/TrackingIcon';

export { default as Truck } from './svg/Truck';
export { default as TruckIcon } from './svg/TruckIcon';
export { default as TruckTaped } from './svg/TruckTaped';
export { default as User } from './svg/User';
export { default as Verified } from './svg/Verified';

export { default as Back } from './svg/Back';
export { default as Cross } from './svg/Cross';