import React from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import styles, { COLORS } from '../../styles';

const component = (props: any) => {
  const bgColor = (!props.backgroundColor) ? COLORS.black : props.backgroundColor;

  const DefaultStatusBar = () => (
    <StatusBar 
      barStyle="light-content" 
      animated={false}
      hidden={false}
      backgroundColor={ bgColor } />
  );
  // backgroundColor={ COLORS.darkblue }
  let container = props.children;

  if (props.keyboardView) {
    container = <KeyboardAwareScrollView>{ props.children }</KeyboardAwareScrollView>
  }

  if (props.scrollView) {
    container = <ScrollView>{ props.children }</ScrollView>
  }

  const screenPadding = (props.screenPadding == "none") ? {paddingLeft:0, paddingRight:0} : {};

  return (
    <SafeAreaView style={[ styles.safeAreaView, { backgroundColor: bgColor }, screenPadding ]}>
      { DefaultStatusBar() }
      { container }
    </SafeAreaView>
  );
};

export default component;