import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import styles from '../../styles';

const component = (props: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();

  const btnHandler = () => {
    navigation.navigate('TermsOfServiceScreen');
  }

  return (
    <View style={[ styles.publicAreaView ]}>
      <View style={[ styles.centerContainer, {maxHeight:200} ]}>
        {/* LOGO */}
      </View>
      <View style={[ ]}>
        { props.children }
      </View>
      <View style={[ styles.footerTermsContainer ]}>
        <Text style={[ styles.footerTerms ]}>{ t('terms_of_service') }</Text>
        <TouchableOpacity onPress={ btnHandler }>
          <Text style={[ styles.footerTerms, styles.textUnderline ]}>{ t('terms_of_service_btn') }</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default component;