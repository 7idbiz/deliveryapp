import React from 'react';
import { Text, View, TouchableOpacity, TouchableHighlight } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import styles from '../../styles';
import {
  Back,
  Search,
  Invoice,
} from '../IconSvg'

const component = (props: any) => {
  const navigation = useNavigation();

  const getIcon = (type: string) => {
    if (type == "back") return <Back />;
    if (type == "search") return <Search />;
    if (type == "order") return <Invoice />;
  }

  const getNav = (type: string) => {
    if (type == "back" && navigation.canGoBack()) navigation.goBack();
    
  }

  const renderIcon = (type: string) => {
    let icon = (!type) 
      ? null 
      : <TouchableOpacity 
          onPress={()=>{ getNav( type ) }} 
          style={[ styles.headerScreenIconContainer ]}>
            { getIcon( type ) }
        </TouchableOpacity>
    
    return <View style={[ {minWidth: 40},  ]}>{ icon }</View>
  }

  return (
    <View style={[ styles.headerScreenContainer ]}>

      { renderIcon(props.leftIcon) }

      <View style={[ {flex:1} ]}>
        <Text style={[ styles.screenTitle ]}>{ props.title }</Text>
      </View>

      { renderIcon(props.rightIcon) }

    </View>
  );
};

export default component;