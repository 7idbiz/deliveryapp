import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import styles from '../../styles';

const component = (props: any) => {
  const customStyle = (props.style && props.style[0]) ? props.style[0] : {};

  return (
    <TouchableOpacity 
      onPress={()=>{ props.onPress && props.onPress() }} 
      style={[ styles.buttonOpacityContainer, customStyle ]}>
      <Text style={[ styles.buttonOpacityTitle ]}>{ props.title }</Text>
    </TouchableOpacity>
  );
};

export default component;