import React, { useState } from 'react';
import { TextInput, Text, View, TouchableOpacity } from 'react-native';

import styles, { COLORS } from '../../styles';
import {
  Orders,
} from '../IconSvg'

const component = (props: any) => {
  const [value, setValue] = useState<string>('');

  const themeStyle = (props.theme && props.theme === "dark") 
    ? styles.inputTextContainerDark : styles.inputTextContainer;

  const themeTextStyle = (props.theme && props.theme === "dark") 
    ? {color: COLORS.white} : {color: COLORS.black};

  const onChange = (val: string)=>{
    props.onChange && props.onChange(val);
    setValue(val);
  };

  const editFlag = (props.rightIcon && props.rightIcon=="edit");

  return (
    <View
      style={[ themeStyle, styles.rowContainer, {paddingLeft:5,paddingRight:0}, (props.error) ? {borderColor:'red'} : {} ]}>
      <Text style={[ styles.inputText, {maxWidth:50, paddingTop:13, color:COLORS.grey} ]}>+48  |</Text>
      <TextInput
        style={[ styles.inputText, themeTextStyle ]}
        onChangeText={onChange}
        value={props.value || value}
        placeholder={props.placeholder}
        keyboardType="numeric"
      />      
      <TouchableOpacity
        // onPress={crossHandler}
        style={[ styles.buttonCross, (editFlag) ? {opacity:1} : {opacity:0} ]}>
        <Orders />
      </TouchableOpacity>
    </View>
  );
};

export default component;