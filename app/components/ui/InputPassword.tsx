import React, { useState } from 'react';
import { TextInput, View, TouchableOpacity } from 'react-native';

import styles, { COLORS } from '../../styles';
import {
  Eye,
} from '../IconSvg'

const component = (props: any) => {
  const [secureFlag, setSecureFlag] = useState<boolean>(true);
  const [value, setValue] = useState<string>('');

  const secureChange = ()=>{
    setSecureFlag(!secureFlag);
  };

  const onChange = (val: string)=>{
    props.onChange && props.onChange(val);
    setValue(val);
  };

  const themeStyle = (props.theme && props.theme === "dark") 
    ? styles.inputTextContainerDark : styles.inputTextContainer;

  const themeTextStyle = (props.theme && props.theme === "dark") 
    ? {color: COLORS.white} : {color: COLORS.black};

  return (
    <View
      style={[ themeStyle, styles.rowContainer, {paddingRight:0}, (props.error) ? {borderColor:'red'} : {}  ]}>
      <TextInput
        style={[ styles.inputText, themeTextStyle ]}
        secureTextEntry={secureFlag} 
        onChangeText={onChange}
        value={props.value || value}
        placeholder={props.placeholder}
        // keyboardType="numeric"
      />
      <TouchableOpacity
        onPress={secureChange}
        style={[ styles.buttonCross, (secureFlag) ? {opacity:1} : {opacity:0.4} ]}>
        <Eye />
      </TouchableOpacity>
      
    </View>
  );
};

export default component;