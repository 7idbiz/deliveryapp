import React, { useState } from 'react';
import { TextInput, Text, View, TouchableOpacity } from 'react-native';

import styles, { COLORS } from '../../styles';
import {
  Tracking,
} from '../IconSvg'

const component = (props: any) => {
  const [value, setValue] = useState<string>('');

  const themeStyle = (props.theme && props.theme === "dark") 
    ? styles.inputTextContainerDark : styles.inputTextContainer;

  const themeTextStyle = (props.theme && props.theme === "dark") 
    ? {color: COLORS.white} : {color: COLORS.black};

  const onChange = (val: string)=>{
    props.onChange && props.onChange(val);
    setValue(val);
  };

  const editFlag = (props.rightIcon && props.rightIcon=="edit");

  return (
    <View
      style={[ themeStyle, styles.rowContainer, {paddingLeft:10, paddingRight:5, marginTop:2, marginBottom:0 }, (props.error) ? {borderColor:'red'} : {} ]}
      >
      <Tracking style={{marginTop:10}} />
      <Text style={[ {width:10, margin:6, fontSize:20, color:COLORS.grey} ]}>|</Text>
      {/* <View style={{width:20, backgroundColor:'red'}} ></View> */}
      <TextInput
        style={[ styles.inputText, themeTextStyle ]}
        onChangeText={onChange}
        value={props.value || value}
        placeholder={props.placeholder}
        keyboardType="numeric"
      />      
      <View style={{width:20}} ></View>
      {/* <TouchableOpacity
        // onPress={crossHandler}
        style={[ styles.buttonCross, (editFlag) ? {opacity:1} : {opacity:0} ]}>
        <Orders />
      </TouchableOpacity> */}
    </View>
  );
};

export default component;