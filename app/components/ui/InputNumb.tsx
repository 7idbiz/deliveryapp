import React, { useState } from 'react';
import { TextInput, View, TouchableOpacity } from 'react-native';

import styles from '../../styles';
import {
  Cross,
} from '../IconSvg'

const component = (props: any) => {
  const [crossFlag, setCrossFlag] = useState<boolean>(false);

  const crossHandler = ()=>{
    // crossFlag && console.log("____cross__");
  };

  const onChange = ()=>{
    // console.log("____change___")
  };

  return (
    <View
      style={[ styles.inputNumbContainer ]}>
      <TextInput
        style={[ styles.inputNumb ]}
        onChangeText={onChange}
        value={props.value}
        placeholder={props.placeholder}
        keyboardType="numeric"
      />
      
    </View>
  );
};

export default component;