import React, { useState } from 'react';
import { TextInput, View, TouchableOpacity } from 'react-native';

import styles, { COLORS } from '../../styles';
import {
  Cross,
} from '../IconSvg'

const component = (props: any) => {
  const [value, setValue] = useState<string>("");
  const [crossFlag, setCrossFlag] = useState<boolean>(false);
  
  const crossHandler = ()=>{
    if (crossFlag) {
      onChange("");
    }
  };

  const onChange = (val: string)=>{
    props.onChange && props.onChange(val);
    setValue(val);
    setCrossFlag( (val.length>0) ? true : false );
  };
  
  const themeStyle = (props.theme && props.theme === "dark") 
    ? styles.inputTextContainerDark : styles.inputTextContainer;

  const themeTextStyle = (props.theme && props.theme === "dark") 
    ? {color: COLORS.white} : {color: COLORS.black};

  const themeIconStyle = (props.theme && props.theme === "dark") 
    ? COLORS.white : COLORS.black;

  return (
    <View
      style={[ themeStyle, styles.rowContainer, {paddingRight:0}, (props.error) ? {borderColor:'red'} : {} ]}>
      <TextInput
        style={[ styles.inputText, themeTextStyle ]}
        onChangeText={onChange}
        value={props.value || value}
        placeholder={props.placeholder}
      />
      <TouchableOpacity
        onPress={crossHandler}
        style={[ styles.buttonCross, (crossFlag) ?{opacity:1} : {opacity:0} ]}>
        <Cross color={ themeIconStyle } />
      </TouchableOpacity>
      
    </View>
  );
};

export default component;