import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import styles from '../../styles';

const component = (props: any) => {
  return (
    <TouchableOpacity 
      onPress={()=>{ props.onPress && props.onPress() }} 
      style={[ styles.buttonOutlineContainer ]}>
      <Text style={[ styles.buttonOutlineTitle ]}>{ props.title }</Text>
    </TouchableOpacity>
  );
};

export default component;