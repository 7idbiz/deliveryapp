import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import styles, { COLORS } from '../../styles';

const component = (props: any) => {
  const customStyle = (props.style && props.style[0]) ? props.style[0] : {};

  return (
    <TouchableOpacity 
      disabled={ props.disabled || false}
      onPress={()=>{ props.onPress && props.onPress() }} 
      style={[ styles.buttonOutlineContainer, {backgroundColor: COLORS.yellow}, customStyle, 
        (props.disabled) ? {opacity:0.5} : {opacity:1} ]}>
      <Text style={[ styles.buttonOutlineTitle, {color: COLORS.black} ]}>{ props.title }</Text>
    </TouchableOpacity>
  );
};

export default component;