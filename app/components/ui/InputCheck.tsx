import React, { useState } from 'react';
import { TextInput, View, Text, TouchableOpacity } from 'react-native';

import styles, { COLORS } from '../../styles';
import {
  Cross,
} from '../IconSvg'

const component = (props: any) => {
  const [partnerFlag, setPartnerFlag] = useState<boolean>(false);

  const onChange = ()=>{
    setPartnerFlag(!partnerFlag);
  };

  return (
    <View
      style={[ styles.inputTextContainer, styles.rowContainer, {padding:0} ]}>
      
      <TouchableOpacity
        onPress={onChange}
        style={[ styles.inputCheckBtn, (!partnerFlag) ? {backgroundColor: COLORS.white} : {backgroundColor: COLORS.yellow}  ]}>
        <Text style={[ styles.buttonOutlineTitle, {color: COLORS.black} ]}>{ props.leftTitle }</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={onChange}
        style={[ styles.inputCheckBtn, (partnerFlag) ? {backgroundColor: COLORS.white} : {backgroundColor: COLORS.yellow} ]}>
        <Text style={[ styles.buttonOutlineTitle, {color: COLORS.black} ]}>{ props.rightTitle }</Text>
      </TouchableOpacity>

    </View>
  );
};

export default component;