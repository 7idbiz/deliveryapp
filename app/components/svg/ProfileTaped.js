import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  const size = (props.style[0] && props.style[0].width) ? props.style[0].width : 20;
  return (
    <Svg 
      width={ size + "px"}
      height={ size + "px"}
      viewBox="0 0 20 20" 
      {...props}>
      <Path
        d="M17.071 12.929a9.962 9.962 0 00-3.8-2.384 5.78 5.78 0 002.51-4.764A5.788 5.788 0 0010 0a5.788 5.788 0 00-5.781 5.781 5.78 5.78 0 002.51 4.764 9.962 9.962 0 00-3.8 2.384A9.935 9.935 0 000 20h1.563c0-4.652 3.785-8.438 8.437-8.438 4.652 0 8.438 3.786 8.438 8.438H20a9.935 9.935 0 00-2.929-7.071zM10 10a4.224 4.224 0 01-4.219-4.219A4.224 4.224 0 0110 1.563a4.224 4.224 0 014.219 4.218A4.224 4.224 0 0110 10z"
        fill={ props.color }
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        opacity={0.7}
      />
    </Svg>
  )
}

export default SvgComponent
