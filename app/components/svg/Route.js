import * as React from "react"
import Svg, { Defs, Circle, G, Use, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function SvgComponent(props) {
  const size = (props.style[0] && props.style[0].width) ? props.style[0].width : 135;

  return (
    <Svg 
      width={ size + "px"} 
      height={ size + "px"} 
      viewBox="0 0 135 135" 
      {...props}>
      <Defs>
        <Circle id="b" cx={37.5} cy={37.5} r={37.5} />
      </Defs>
      <G stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
        <G
          fillRule="nonzero"
          transform="translate(-120 -667) translate(150 693)"
        >
          <Use fill="#000" filter="url(#a)" xlinkHref="#b" />
          <Use fill="#F9CF1C" xlinkHref="#b" />
        </G>
        <G
          transform="translate(-120 -667) translate(150 693) translate(22 18)"
          fill="#171B26"
        >
          <Circle cx={7.68292683} cy={7.68292683} r={2.56097561} />
          <Path
            d="M13.116 12.676c1.45-1.402 2.25-3.267 2.25-5.25C15.366 3.33 11.919 0 7.683 0S0 3.331 0 7.426c0 1.983.8 3.848 2.25 5.25l5.433 5.25 5.433-5.25zm-9.973-5.25c0-2.42 2.037-4.388 4.54-4.388s4.54 1.968 4.54 4.388-2.037 4.387-4.54 4.387-4.54-1.968-4.54-4.387z"
            fillRule="nonzero"
          />
          <Path d="M29.451 21.341c-3.06 0-5.549 2.538-5.549 5.658 0 1.511.578 2.932 1.626 4L29.45 35l3.924-4A5.677 5.677 0 0035 26.999c0-3.12-2.49-5.658-5.549-5.658z" />
          <Path d="M9.448 29.396c0-3.103 2.607-5.627 5.81-5.627h4.86c2.878 0 5.219-2.268 5.219-5.055 0-2.788-2.341-5.055-5.22-5.055h-5.08v2.537h5.081c1.433 0 2.6 1.13 2.6 2.518s-1.167 2.518-2.6 2.518h-4.86c-4.648 0-8.429 3.663-8.429 8.164 0 4.502 3.781 8.165 8.43 8.165h14.619v-2.537h-14.62c-3.203 0-5.81-2.525-5.81-5.628z" />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
