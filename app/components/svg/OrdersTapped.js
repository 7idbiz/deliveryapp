import * as React from "react"
import Svg, { G, Path, Circle } from "react-native-svg"

function SvgComponent(props) {
  const size = (props.style[0] && props.style[0].width) ? props.style[0].width : 20;
  return (
    <Svg 
      width={ size + "px"}
      height={ size + "px"} 
      viewBox="0 0 20 20" 
      {...props}>
      <G
        fill={ props.color }
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        opacity={0.7}
      >
        <Path d="M15.625 0H4.375A4.375 4.375 0 000 4.375v11.25A4.375 4.375 0 004.375 20h11.25A4.375 4.375 0 0020 15.625V4.375A4.375 4.375 0 0015.625 0zm3.125 15.625c0 1.726-1.4 3.125-3.125 3.125H4.375a3.125 3.125 0 01-3.125-3.125V4.375c0-1.726 1.4-3.125 3.125-3.125h11.25c1.726 0 3.125 1.4 3.125 3.125v11.25z" />
        <Path d="M8 5H15V6H8z" />
        <Path d="M8 9H15V10H8z" />
        <Path d="M8 13H15V14H8z" />
        <Circle cx={5} cy={5} r={1} />
        <Circle cx={5} cy={9} r={1} />
        <Circle cx={5} cy={13} r={1} />
      </G>
    </Svg>
  )
}

export default SvgComponent
