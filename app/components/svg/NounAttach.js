import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="22px" height="23px" viewBox="0 0 22 23" {...props}>
      <Path
        d="M8.275 15.507a.826.826 0 000 1.173.826.826 0 001.173 0l7.818-7.819c1.369-1.368 1.369-3.649 0-5.082-1.433-1.368-3.714-1.368-5.082 0l-10.75 10.75a5.076 5.076 0 000 7.038 5.076 5.076 0 007.036 0l10.751-10.751c2.476-2.476 2.476-6.516 0-8.992-2.476-2.41-6.516-2.41-8.992 0L.456 11.598a.826.826 0 000 1.173.826.826 0 001.173 0l9.773-9.774c1.825-1.824 4.822-1.824 6.646 0 1.825 1.825 1.825 4.822 0 6.646l-10.75 10.75a3.303 3.303 0 01-4.692 0 3.303 3.303 0 010-4.69l10.75-10.751c.718-.717 1.956-.717 2.737 0 .717.782.717 2.02 0 2.736l-7.818 7.82z"
        transform="translate(-23 -406) translate(23 397) translate(0 9)"
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fillOpacity={0.3}
      />
    </Svg>
  )
}

export default SvgComponent
