import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="16px" height="20px" viewBox="0 0 16 20" {...props}>
      <G fill="#AEAEAE" fillRule="nonzero" stroke="none" strokeWidth={1}>
        <Path
          d="M15.137 0H.497A.498.498 0 000 .498v19.004a.498.498 0 00.835.366l1.732-1.597a.496.496 0 01.669-.005l1.135 1.013a1.491 1.491 0 001.994-.005l1.119-1.01a.496.496 0 01.667 0l1.119 1.01a1.49 1.49 0 001.994.005l1.135-1.013c.191-.172.481-.17.67.005l1.73 1.597a.498.498 0 00.836-.366V.498A.498.498 0 0015.137 0zm-.498 18.366l-.896-.827a1.488 1.488 0 00-2.007-.016l-1.135 1.013a.497.497 0 01-.665-.002l-1.118-1.009a1.49 1.49 0 00-2 0l-1.12 1.01a.497.497 0 01-.664.001l-1.135-1.013a1.488 1.488 0 00-2.007.016l-.896.827V.996h13.643v17.37z"
          transform="translate(-335 -57) translate(335 57)"
        />
        <Path
          d="M4.49 7.754h6.388a.498.498 0 100-.996H4.49a.498.498 0 100 .996zM4.49 11.214h6.388a.498.498 0 100-.996H4.49a.498.498 0 100 .996z"
          transform="translate(-335 -57) translate(335 57)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
