import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="20px" height="20px" viewBox="0 0 20 20" {...props}>
      <Path
        d="M19.26.956L15.355.052a.911.911 0 00-1.03.518l-1.8 4.2a.904.904 0 00.26 1.052l2.273 1.86a13.852 13.852 0 01-6.666 6.666l-1.86-2.267a.896.896 0 00-1.052-.259l-4.2 1.8a.904.904 0 00-.525 1.03l.94 3.867c.091.409.455.699.874.696A17.393 17.393 0 0020 1.83a.889.889 0 00-.74-.874z"
        transform="translate(-311 -519) translate(23 503) translate(288 16)"
        fill="#F9CF1C"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
      />
    </Svg>
  )
}

export default SvgComponent
