import * as React from "react"
import Svg, { G, Circle, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="21px" height="21px" viewBox="0 0 21 21" {...props}>
      <G
        transform="translate(-315 -635) translate(315 635)"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fill="none"
      >
        <Circle
          fill="#FFF"
          opacity={0.0968424479}
          cx={10.5}
          cy={10.5}
          r={10.5}
        />
        <Path
          stroke="#F6CF28"
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M6 11.0452617L9.40496056 14 15.8496094 7.56152344"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
