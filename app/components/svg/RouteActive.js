import * as React from "react"
import Svg, { Defs, Circle, G, Use, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: filter */

function SvgComponent(props) {
  return (
    <Svg width="137px" height="137px" viewBox="0 0 137 137" {...props}>
      <Defs>
        <Circle id="b" cx={37.5} cy={37.5} r={37.5} />
      </Defs>
      <G stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
        <G
          fillRule="nonzero"
          transform="translate(-119 -666) translate(150 693)"
        >
          <Use fill="#000" filter="url(#a)" xlinkHref="#b" />
          <Use fill="#F9C40C" xlinkHref="#b" />
          <Use fill="#000" filter="url(#c)" xlinkHref="#b" />
          <Use stroke="#F2B617" xlinkHref="#b" />
        </G>
        <G
          transform="translate(-119 -666) translate(150 693) translate(19 16)"
          fill="#FFF"
        >
          <Circle cx={9} cy={9} r={3} />
          <Path
            d="M15.364 14.85C17.064 13.205 18 11.021 18 8.697 18 3.902 13.963 0 9 0S0 3.902 0 8.698c0 2.324.936 4.508 2.636 6.151L9 21l6.364-6.15zM3.682 8.698c0-2.835 2.386-5.14 5.318-5.14 2.932 0 5.318 2.305 5.318 5.14 0 2.834-2.386 5.14-5.318 5.14-2.932 0-5.318-2.306-5.318-5.14z"
            fillRule="nonzero"
          />
          <Path d="M34.5 25c-3.584 0-6.5 2.973-6.5 6.627a6.65 6.65 0 001.904 4.687L34.5 41l4.596-4.686A6.65 6.65 0 0041 31.627C41 27.973 38.084 25 34.5 25z" />
          <Path d="M11.068 34.436c0-3.635 3.053-6.592 6.806-6.592h5.693c3.37 0 6.113-2.657 6.113-5.922 0-3.266-2.742-5.922-6.113-5.922h-5.953v2.972h5.953c1.679 0 3.045 1.323 3.045 2.95 0 1.626-1.366 2.95-3.045 2.95h-5.693c-5.445 0-9.874 4.29-9.874 9.564S12.43 44 17.874 44H35v-2.972H17.874c-3.753 0-6.806-2.957-6.806-6.592z" />
        </G>
      </G>
    </Svg>
  )
}

export default SvgComponent
