import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="23px" height="23px" viewBox="0 0 23 23" {...props}>
      <Path
        d="M5.018 9.055h10.117a.608.608 0 100-1.217H5.018a.608.608 0 100 1.217zm7.282 3.618H5.018a.608.608 0 100 1.217H12.3a.608.608 0 000-1.217zm9.832 5.387a4.438 4.438 0 01-8.303 2.185H3.992a3.452 3.452 0 01-3.448-3.448V3.663A3.452 3.452 0 013.992.215h12.17a3.452 3.452 0 013.447 3.448V14.06a4.442 4.442 0 012.523 4zm-8.773.968a4.415 4.415 0 015.033-5.348V3.663a2.234 2.234 0 00-2.23-2.231H3.991c-1.232.001-2.23 1-2.231 2.23v13.135c.001 1.232 1 2.23 2.23 2.231h9.368zm7.556-.968a3.224 3.224 0 10-6.448 0 3.224 3.224 0 006.448 0z"
        transform="translate(-20 -252) translate(20 252)"
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fillOpacity={0.3}
      />
    </Svg>
  )
}

export default SvgComponent
