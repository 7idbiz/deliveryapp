import * as React from "react"
import Svg, { G, Circle, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="20px" height="20px" viewBox="0 0 20 20" {...props}>
      <G
        transform="translate(-39 -613) translate(40 614)"
        fillRule="nonzero"
        stroke="#FFF"
        strokeWidth={1}
        fill="none"
        opacity={0.3}
      >
        <Circle strokeWidth={1.1} cx={9} cy={9} r={9} />
        <Path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M8.59765625 3L8.59765625 10.3374023 13.1948242 10.3374023" 
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
