import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="20px" height="20px" viewBox="0 0 20 20" {...props}>
      <Path
        d="M17.071 2.929A9.935 9.935 0 0010 0a9.935 9.935 0 00-7.071 2.929A9.935 9.935 0 000 10a9.935 9.935 0 002.929 7.071A9.935 9.935 0 0010 20a9.935 9.935 0 007.071-2.929A9.935 9.935 0 0020 10a9.935 9.935 0 00-2.929-7.071zm-12.73 13.84c.33-2.84 2.772-5.034 5.659-5.034 1.522 0 2.953.593 4.03 1.669a5.694 5.694 0 011.63 3.365A8.788 8.788 0 0110 18.83a8.788 8.788 0 01-5.66-2.06zM10 10.528A3.04 3.04 0 016.963 7.49 3.04 3.04 0 0110 4.453a3.04 3.04 0 013.037 3.037A3.04 3.04 0 0110 10.528zm6.669 5.25a6.871 6.871 0 00-1.81-3.203 6.85 6.85 0 00-2.502-1.599 4.208 4.208 0 001.852-3.486c0-2.32-1.888-4.209-4.209-4.209a4.208 4.208 0 00-2.355 7.696 6.854 6.854 0 00-2.327 1.428 6.849 6.849 0 00-1.987 3.373A8.79 8.79 0 011.17 10c0-4.868 3.961-8.828 8.829-8.828s8.828 3.96 8.828 8.828a8.79 8.79 0 01-2.16 5.779z"
        transform="translate(-39 -707) translate(39 707)"
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        opacity={0.3}
      />
    </Svg>
  )
}

export default SvgComponent

