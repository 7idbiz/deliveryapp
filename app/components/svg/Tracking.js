import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="21px" height="21px" viewBox="0 0 21 21" {...props}>
      <G fill="#FFF" fillRule="nonzero" stroke="none" strokeWidth={1}>
        <Path
          d="M5.814 13.23L.48 14.758c-.605.173-.65 1.01-.066 1.245l2.435.983.992 2.41c.238.58 1.083.535 1.258-.065l1.54-5.282a.664.664 0 00-.825-.817zm-1.463 3.906l-.373-.907a.664.664 0 00-.366-.362l-.916-.37 2.327-.666-.672 2.305z"
          transform="translate(-27 -531) translate(0 379) translate(10 10) translate(0 75) translate(17 67.34)"
        />
        <Path
          d="M17.284 9.284H5.216c-.832 0-1.51-.694-1.51-1.547 0-.854.678-1.548 1.51-1.548h12.533c.209.37.598.62 1.044.62.666 0 1.207-.556 1.207-1.239 0-.457-.243-.857-.603-1.07V.618A.611.611 0 0018.793 0h-2.715a.611.611 0 00-.604.619v2.166c0 .342.27.62.604.62h2.112v1.094a1.232 1.232 0 00-.441.452H5.216c-1.498 0-2.716 1.25-2.716 2.786 0 1.535 1.218 2.785 2.716 2.785h12.068c.832 0 1.51.694 1.51 1.547s-.678 1.548-1.51 1.548H8.233a.611.611 0 00-.604.618c0 .342.27.62.604.62h9.051c1.498 0 2.716-1.25 2.716-2.786s-1.218-2.785-2.716-2.785zm-.603-7.118v-.928h1.509v.928H16.68z"
          stroke="#FFF"
          strokeWidth={0.5}
          transform="translate(-27 -531) translate(0 379) translate(10 10) translate(0 75) translate(17 67.34)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
