import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="20px" height="20px" viewBox="0 0 20 20" {...props}>
      <Path
        d="M8.862 0a8.854 8.854 0 016.653 14.685l4.3 4.3A.577.577 0 1119 19.8l-4.3-4.3A8.846 8.846 0 118.862 0zm0 1.154a7.692 7.692 0 107.692 7.692A7.692 7.692 0 008.862 1.17v-.015z"
        transform="translate(-332 -57) translate(332 57)"
        fill="#F9CF1C"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
      />
    </Svg>
  )
}

export default SvgComponent
