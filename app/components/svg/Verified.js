import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="20px" height="23px" viewBox="0 0 20 23" {...props}>
      <Path
        d="M18.61 3.676L9.917.068a.887.887 0 00-.68 0L.547 3.676a.887.887 0 00-.547.82v4.386c0 6.04 3.65 11.473 9.243 13.756a.887.887 0 00.67 0 14.852 14.852 0 009.243-13.756V4.495a.887.887 0 00-.547-.819zm-1.227 5.206c0 5.135-3.016 9.862-7.805 11.972A13.082 13.082 0 011.774 8.882V5.087l7.804-3.24 7.805 3.24v3.795zm-8.785 2.99L12.41 8.06a.887.887 0 111.254 1.254l-4.439 4.439a.887.887 0 01-1.254 0l-2.479-2.479a.887.887 0 111.254-1.254l1.852 1.851z"
        transform="translate(-23 -305) translate(23 305)"
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fillOpacity={0.3}
      />
    </Svg>
  )
}

export default SvgComponent
