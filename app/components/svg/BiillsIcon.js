import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="23px" height="23px" viewBox="0 0 23 23" {...props}>
      <G
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fillOpacity={0.3}
      >
        <Path
          d="M22.115.442H.885a.442.442 0 00-.443.443v21.23c0 .245.198.443.443.443h21.23a.442.442 0 00.443-.443V.885a.442.442 0 00-.443-.443zm-.442 21.231H1.327V1.327h20.346v20.346z"
          transform="translate(-20 -384) translate(20 384)"
        />
        <Path
          d="M3.538 3.98h.885a.442.442 0 100-.884h-.885a.442.442 0 000 .885zM7.077 3.98h.885a.442.442 0 000-.884h-.885a.442.442 0 100 .885zM10.615 3.98h.885a.442.442 0 100-.884h-.885a.442.442 0 100 .885zM9.587 11.5H8.404a.442.442 0 000 .885H9.4c-.02.146-.034.294-.038.442.004.148.017.296.038.442h-.997a.442.442 0 000 .885h1.183a3.059 3.059 0 002.785 2.211h1.782a.442.442 0 000-.884h-1.782a2.093 2.093 0 01-1.83-1.327h2.285a.442.442 0 100-.885h-2.544a2.75 2.75 0 010-.884h2.544a.442.442 0 100-.885h-2.284a2.093 2.093 0 011.83-1.327h1.78a.442.442 0 000-.885h-1.78A3.059 3.059 0 009.586 11.5z"
          transform="translate(-20 -384) translate(20 384)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
