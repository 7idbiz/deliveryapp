import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="20px" height="20px" viewBox="0 0 20 20" {...props}>
      <G
        transform="translate(-226 -636) translate(0 379) translate(10 10) translate(216 247)"
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
      >
        <Path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0zm0 19c-4.963 0-9-4.037-9-9s4.037-9 9-9 9 4.037 9 9-4.037 9-9 9z" />
        <Path d="M10.46875 5L9.53125 5 9.53125 9.53125 5 9.53125 5 10.46875 9.53125 10.46875 9.53125 15 10.46875 15 10.46875 10.46875 15 10.46875 15 9.53125 10.46875 9.53125z" />
      </G>
    </Svg>
  )
}

export default SvgComponent
