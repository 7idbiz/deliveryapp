import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="14px" height="8px" viewBox="0 0 14 8" {...props}>
      <Path
        transform="translate(-92 -139) translate(0 116) translate(92 23)"
        fill="#F9CF1C"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        d="M14.0000031 3.97020541L9.47060574 7.8620973 9.47060574 5.69993514 -0.00000308823529 5.69993514 -0.00000308823529 2.24047351 9.47060574 2.24047351 9.47060574 0.0783113514z"
      />
    </Svg>
  )
}

export default SvgComponent
