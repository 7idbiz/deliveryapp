import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width={11.5} height={21.5} viewBox="0 0 11.5 21.5" {...props}>
      <Path
        fill="none"
        stroke="#F9CF1C"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1.5}
        d="M10.75 20.75l-10-10 10-10"
      />
    </Svg>
  )
}

export default SvgComponent
