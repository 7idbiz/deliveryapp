import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="23px" height="23px" viewBox="0 0 23 23" {...props}>
      <G
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fillOpacity={0.3}
      >
        <Path
          d="M11.473 6.525a3.643 3.643 0 110 7.286 3.643 3.643 0 010-7.286zm0 1.087a2.553 2.553 0 00-2.556 2.556 2.553 2.553 0 002.556 2.555c1.414 0 2.61-1.141 2.61-2.555 0-1.414-1.196-2.556-2.61-2.556z"
          transform="translate(-20 -186) translate(20 186)"
        />
        <Path
          d="M11.473 0C17.835 0 23 5.165 23 11.473 23 17.835 17.835 23 11.473 23 5.165 23 0 17.835 0 11.473 0 5.165 5.165 0 11.473 0zm0 1.142a10.326 10.326 0 00-10.331 10.33c0 5.71 4.622 10.332 10.33 10.332 5.71 0 10.332-4.622 10.332-10.331 0-5.71-4.622-10.331-10.331-10.331z"
          transform="translate(-20 -186) translate(20 186)"
        />
        <Path
          d="M8.645 14.191H14.3a3.367 3.367 0 013.371 3.263l-1.033.924v-.815c0-1.25-1.033-2.284-2.338-2.284H8.645a2.297 2.297 0 00-2.283 2.284v.815l-1.088-.924c.109-1.794 1.577-3.263 3.371-3.263z"
          transform="translate(-20 -186) translate(20 186)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
