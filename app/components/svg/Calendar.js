import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="17px" height="17px" viewBox="0 0 17 17" {...props}>
      <G
        fill="#FFF"
        stroke="none"
        strokeWidth={1}
        fillRule="evenodd"
        opacity={0.3}
      >
        <Path
          d="M15.179 1.821h-1.822V.607a.607.607 0 10-1.214 0v1.214H4.857V.607a.607.607 0 10-1.214 0v1.214H1.82A1.821 1.821 0 000 3.643v11.536C0 16.185.815 17 1.821 17H15.18A1.821 1.821 0 0017 15.179V3.643a1.821 1.821 0 00-1.821-1.822zm.607 13.358a.607.607 0 01-.607.607H1.82a.607.607 0 01-.607-.607V7.286h14.572v7.893zm0-9.108H1.214V3.643c0-.335.272-.607.607-.607h1.822V4.25a.607.607 0 101.214 0V3.036h7.286V4.25a.607.607 0 101.214 0V3.036h1.822c.335 0 .607.272.607.607V6.07z"
          fillRule="nonzero"
          transform="translate(-40 -460) translate(23 420) translate(17 40)"
        />
        <Path
          d="M11.847 9.14a.501.501 0 00-.696 0l-3.653 3.652-1.65-1.649a.501.501 0 00-.707.708l2.003 2.002a.501.501 0 00.708 0l4.007-4.005a.5.5 0 00-.012-.707z"
          transform="translate(-40 -460) translate(23 420) translate(17 40)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
