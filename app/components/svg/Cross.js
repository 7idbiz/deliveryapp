import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="17px" height="17px" viewBox="0 0 490 490" {...props}>
      <G fill={ props.color || "#3333FF"} fillRule="nonzero" stroke="none" strokeWidth={1}> 
        <Path d="M11.387 490L245 255.832 478.613 490 489.439 479.174 255.809 244.996 489.439 10.811 478.613 0 245 234.161 11.387 0 0.561 10.811 234.191 244.996 0.561 479.174z" />
      </G>
    </Svg>
  )
}

export default SvgComponent
