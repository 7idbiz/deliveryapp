import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="30px" height="18px" viewBox="0 0 30 18" {...props}>
      <G fill="#AEAEAE" fillRule="nonzero" stroke="none" strokeWidth={1}>
        <Path
          d="M14.025 18c-7.6 0-13.6-8.1-13.8-8.4-.3-.3-.3-.8 0-1.2.2-.3 6.2-8.4 13.8-8.4 7.6 0 13.6 8.1 13.8 8.4.3.3.3.8 0 1.2-.2.3-6.2 8.4-13.8 8.4zm-11.7-9c1.5 1.8 6.2 7 11.7 7 5.5 0 10.3-5.2 11.7-7-1.5-1.8-6.2-7-11.7-7-5.5 0-10.3 5.2-11.7 7z"
          transform="translate(-307 -178) translate(307.975 178)"
        />
        <Path
          d="M14.025 14c-2.8 0-5-2.2-5-5s2.2-5 5-5 5 2.2 5 5-2.2 5-5 5zm0-8c-1.7 0-3 1.3-3 3s1.3 3 3 3 3-1.3 3-3-1.3-3-3-3z"
          transform="translate(-307 -178) translate(307.975 178)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
