import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="40px" height="35px" viewBox="0 0 40 35" {...props}>
      <G
        fill="#FFF"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
        fillOpacity={0.3}
      >
        <Path
          d="M35.037 5.328h-6.183L26.96 1.429A2.535 2.535 0 0024.69 0h-9.422c-.967 0-1.85.563-2.271 1.43l-1.893 3.898H4.921C2.187 5.328 0 7.58 0 10.353v19.579C0 32.704 2.187 35 4.921 35H35.08C37.77 35 40 32.748 40 29.932v-19.58c-.042-2.772-2.23-5.024-4.963-5.024zM19.979 28.936c-4.963 0-9.001-4.159-9.001-9.27 0-5.112 4.038-9.27 9.001-9.27 4.963 0 9.001 4.158 9.001 9.27 0 5.111-4.038 9.27-9.001 9.27z"
          transform="translate(-84 -640) translate(23 596) translate(28 14) translate(33 30)"
        />
        <Path
          d="M24.47 17.33h-2.44v-2.66c0-.948-.703-1.67-1.53-1.67-.868 0-1.53.767-1.53 1.67v2.66h-2.44c-.868 0-1.53.768-1.53 1.67 0 .947.703 1.67 1.53 1.67h2.44v2.66c0 .948.703 1.67 1.53 1.67.868 0 1.53-.767 1.53-1.67v-2.66h2.44c.868 0 1.53-.768 1.53-1.67 0-.947-.662-1.67-1.53-1.67z"
          transform="translate(-84 -640) translate(23 596) translate(28 14) translate(33 30)"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
