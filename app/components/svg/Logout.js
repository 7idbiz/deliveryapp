import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg width="23px" height="21px" viewBox="0 0 23 21" {...props}>
      <Path
        d="M1.917.447C.866.447 0 1.285 0 2.3v16.06c0 1.015.866 1.852 1.917 1.852h11.5c1.05 0 1.916-.837 1.916-1.853v-4.323h-1.277v4.323c0 .353-.274.618-.64.618h-11.5c-.364 0-.638-.265-.638-.618V2.3c0-.352.274-.617.639-.617h11.5c.365 0 .639.265.639.617v5.56h1.277V2.3c0-1.015-.866-1.853-1.916-1.853h-11.5zm16.67 6.416l-.903.873 2.743 2.652h-9.958v1.235h9.958l-2.743 2.652.904.873 4.285-4.143-4.285-4.142z"
        transform="translate(-20 -523) translate(20 523)"
        fill="#FFF"
        stroke="none"
        strokeWidth={1}
        fillRule="evenodd"
        fillOpacity={0.3}
      />
    </Svg>
  )
}

export default SvgComponent
