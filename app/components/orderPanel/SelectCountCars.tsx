import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles, { COLORS } from '../../styles';
import {
  Plus,
} from '../IconSvg'

const component = (props: any) => {
  const [count, setCount] = useState<number>(1);

  const changeCountHandler = (state: string) => {
    if (state==="add") {
      setCount(count+1);
    } else if (count>1) {
      setCount(count-1);
    }
  }

  const addDestinationHandler = () => {
    console.log("Add destination")
  }

  return (
    <View style={[ styles.orderPanelSelectDay ]}>
      <View style={[ styles.orderPanelSelectDayButton ]}>
        <TouchableOpacity
          onPress={()=>changeCountHandler("")}
          style={[ styles.orderPanelSelectCountCar ]}>
          <Text style={[ styles.orderPanelSelectDayButtonLabel ]}>-</Text>
        </TouchableOpacity>
        <Text style={[ styles.orderPanelSelectDayButtonLabel ]}>{ (count==1) ? '1 car' : count + ' cars' }</Text>
        <TouchableOpacity
          onPress={()=>changeCountHandler('add')}
          style={[ styles.orderPanelSelectCountCar ]}>
          <Text style={[ styles.orderPanelSelectDayButtonLabel ]}>+</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={addDestinationHandler}
        style={[ styles.orderPanelSelectCountCar ]}
      >
        <Plus style={{ paddingRight: 5 }} />
        <Text style={[ styles.orderPanelSelectDayButtonLabel ]}>Add destination</Text>
      </TouchableOpacity>
    </View>
  );
};

export default component;