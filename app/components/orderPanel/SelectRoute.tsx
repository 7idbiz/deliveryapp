import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles, { COLORS } from '../../styles';
import {
  InputRoute,
} from '../../components'

const component = (props: any) => {
  const { t } = useTranslation();
  // const [visibleFlag, setVisibleFlag] = useState<boolean>(false);

  return (
    <View style={[ { marginLeft:7, marginRight:7 } ]}>
      <InputRoute 
        // value={mobileInput} onChange={setMobileInput} error={mobileError}
        placeholder="From" theme="dark" />
      <InputRoute 
        // value={mobileInput} onChange={setMobileInput} error={mobileError}
        placeholder="To" theme="dark" />
    </View>
  );
};

export default component;