import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles, { COLORS } from '../../styles';
import {
  ButtonFilled,
  SelectCarType,
  SelectCountCars,
  SelectDay,
  SelectRoute,
} from '..'

const component = (props: any) => {
  // const [visibleFlag, setVisibleFlag] = useState<boolean>(false);
  const { t } = useTranslation();

  useEffect(()=>{
    
  }, [props.visibleFlag]);

  const hideHandler = () => {
    props.setVisibleFlag && props.setVisibleFlag(false);
  };

  const btnCalculateHandler = () => {
    console.log("___btnCalculateHandler___");
  };

  if (!props.visibleFlag) {
    return <View></View>;
  }

  return (
    <View style={[ styles.orderPanelContainer ]}>
      <TouchableOpacity 
        onPress={hideHandler}
        style={{flex:1}} />

      <View style={[ styles.orderFormContainer ]}>
        <SelectCarType />
        <SelectDay />
        <SelectRoute /> 
        <SelectCountCars />
        <ButtonFilled 
          disabled={true}
          title={ t('title_calculate') } 
          onPress={btnCalculateHandler} 
          style={[ {marginLeft:7, marginRight:7} ]}/>
      </View>
    </View>
  );
};

export default component;