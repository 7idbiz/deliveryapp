import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';

import styles, { COLORS } from '../../styles';
import {
  Route,
} from '../IconSvg'

const component = (props: any) => {
  // const [visibleFlag, setVisibleFlag] = useState<boolean>(false);

  const changeDateHandler = () => {
    console.log('change date');
  }

  return (
    <View style={[ styles.orderPanelSelectDay ]}>
      <Text style={[ styles.orderPanelSelectDayLabel ]}>Today</Text>
      <TouchableOpacity
        onPress={changeDateHandler}
        style={[ styles.orderPanelSelectDayButton ]}
      >
        <Text style={[ styles.orderPanelSelectDayButtonLabel ]}>Change date</Text>
      </TouchableOpacity>
    </View>
  );
};

export default component;