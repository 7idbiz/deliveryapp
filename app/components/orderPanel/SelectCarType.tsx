import React, { useState } from 'react';
import { Text, View, TouchableOpacity, ImageBackground } from 'react-native';

import styles, { COLORS } from '../../styles';
import {
  Route,
} from '../IconSvg'

const component = (props: any) => {
  const [carType, setCarType] = useState<number>(-1);
  
  const selectCarType = (item: any)=>{
    console.log("___select___", item)
  }

  const config = [{
    label: 'Economy',
    price: '1km x €0.8',
    text: '9 cars max',
    image: require( '../img/car-economy.jpg' ),
  }, {
    label: 'Comfort',
    price: '1km x €1.0',
    text: '2 cars only',
    image: require( '../img/car-comfort.jpg' ),
  }, {
    label: 'Premium',
    price: '1km x €1.2',
    text: '2 cars only',
    image: require( '../img/car-premium.jpg' ),
  }];

  return (
    <View style={[ { flexDirection:'row' } ]}>
      { config.map( (item, id) => {
        return (
          <TouchableOpacity 
            key={ id } style={[ styles.orderPanelSelectCarTypeBg ]}
            // onPress={selectCarType}
            onPress={()=>setCarType(id)} >
          <ImageBackground source={ item.image } 
            imageStyle={{ borderRadius:7 }}
            style={[ {flex:1}, ( carType == id || carType == -1 ) ? {opacity:1} : {opacity:0.5} ]}>
              <View style={[ styles.orderPanelSelectCarTypeItem ]}>
                <Text style={[ styles.orderPanelSelectCarTypeItemTitle ]}>{item.label}</Text>
                <Text style={[ styles.orderPanelSelectCarTypeItemLabel ]}>{item.price}</Text>
                <Text style={[ styles.orderPanelSelectCarTypeItemLabel ]}>{item.text}</Text>
              </View>
          </ImageBackground>
          </TouchableOpacity>
        )

      }) }
    </View>
  );
};

export default component;