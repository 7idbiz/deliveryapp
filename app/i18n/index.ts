import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import en from "./en.json";
/*
let count = 0;
setInterval( ()=> {
  count++;
  const lang = (count==1) ? "ru" : (count==2) ? "ua" : "en";
  if(count>=3) count=0;
  i18n.changeLanguage(lang)
}, 2000)
*/
i18n
  .use(initReactI18next)
  .init({
    resources: {
      en: en
    },
    lng: "en",
    fallbackLng: "en",

    interpolation: {
      escapeValue: false
    }
  });