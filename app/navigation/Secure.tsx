import React from 'react';

import {
  createStack,
  createTabs,
} from './NavUtils';

import styles, { COLORS } from '../styles';
import {
  IconsScreen,
  FontsScreen,
  EventsScreen,
  EventDetailsScreen,

  MainScreen,
  OrdersScreen,
  OrderDetailsScreen,

  ProfileScreen,
  ProfileForgotScreen,
  ProfilePasswordScreen,

  CarrierProfileScreen,
  ClientProfileScreen,
} from '../screens';
import {
  HomeActive,
  OrdersTapped,
  BellTapped,
  ProfileTaped,
} from '../components/IconSvg'

import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();  

// *********************************** MAIN TAB **********************************
const mainTabInitialRouteName = "mainTabScreen";
const mainTabStackScreens = [{
  name: "mainTabScreen",
  screen: MainScreen,
  options: {headerShown: false}
// }, {
//   name: "mainTabScreen",
//   screen: MainScreen,
//   options: {headerShown: false}
// }, {
//   name: "mainTabScreen",
//   screen: MainScreen,
//   options: {headerShown: false}
// }, {
//   name: "mainTabScreen",
//   screen: MainScreen,
//   options: {headerShown: false}
// }, {
//   name: "mainTabScreen",
//   screen: MainScreen,
//   options: {headerShown: false}
}];
const mainTab = () => createStack( mainTabInitialRouteName, mainTabStackScreens );


// *********************************** ORDERS TAB **********************************
const ordersTabInitialRouteName = "ordersTabScreen";
const ordersTabStackScreens = [{
  name: "ordersTabScreen",
  screen: OrdersScreen,
  options: {headerShown: false}
}, {
  name: "OrderDetailsScreen",
  screen: OrderDetailsScreen,
  options: {headerShown: false}
// }, {
//   name: "ordersTabScreen",
//   screen: OrdersScreen,
//   options: {headerShown: false}
// }, {
//   name: "ordersTabScreen",
//   screen: OrdersScreen,
//   options: {headerShown: false}
}];
const ordersTab = () => createStack( ordersTabInitialRouteName, ordersTabStackScreens );


// *********************************** EVENTS TAB **********************************
const eventsTabInitialRouteName = "eventsTabScreen";
const eventsTabStackScreens = [{
  name: "eventsTabScreen",
  screen: EventsScreen,
  // screen: IconsScreen,
  options: {headerShown: false}
}, {
  name: "EventDetailsScreen",
  screen: EventDetailsScreen,
  options: {headerShown: false}
// }, {
//   name: "eventsTabScreen",
//   screen: EventsScreen,
//   options: {headerShown: false}
// }, {
//   name: "IconsScreen",
//   screen: IconsScreen,
//   options: {headerShown: false}
}];
const eventsTab = (props: any) => createStack( eventsTabInitialRouteName, eventsTabStackScreens );



// navigationOptions: {
//   tabBarLabel:"Home",
//   tabBarIcon: ({ tintColor }) => (
//     <Icon name="ios-bookmarks" size={20}/>
//   )
// },

// *********************************** PROFILE TAB **********************************
const profileTabInitialRouteName = "profileTabScreen";
const profileTabStackScreens = [{
  name: "profileTabScreen",
  screen: ProfileScreen,
  options: {headerShown: false}
}, {
  name: "CarrierProfileScreen",
  screen: CarrierProfileScreen,
  options: {headerShown: false}
}, {
  name: "ClientProfileScreen", 
  screen: ClientProfileScreen,
  options: {headerShown: false}
},{
  name: "ProfileForgotScreen",
  screen: ProfileForgotScreen,
  options: {headerShown: false}
},{
  name: "ProfilePasswordScreen",
  screen: ProfilePasswordScreen,
  options: {headerShown: false}
},{
  name: "IconsScreen",
  screen: IconsScreen,
  options: {headerShown: false}
},{
  name: "FontsScreen",
  screen: FontsScreen,
  options: {headerShown: false}
// },{
//   name: "profileTabScreen",
//   screen: ProfileScreen,
//   options: {headerShown: false}
// },{
//   name: "profileTabScreen",
//   screen: ProfileScreen,
//   options: {headerShown: false}
// },{
//   name: "profileTabScreen",
//   screen: ProfileScreen,
//   options: {headerShown: false}
}];
const profileTab = () => createStack( profileTabInitialRouteName, profileTabStackScreens );


// *********************************** TAB NAVIGATOR **********************************
const initialRouteName = "mainTab"; 
const tabScreens = [{
  name: "mainTab",
  component: mainTab,
  options:{
    tabBarLabel:"",
    tabBarIcon: (props: any) => (
      <HomeActive style={[ styles.tabIconStyle ]} color={props.color} />
    )
  },
}, { 
  name: "ordersTab",
  component: ordersTab,
  options:{ 
    tabBarLabel:"",
    tabBarIcon: (props: any) => (
      <OrdersTapped style={[ styles.tabIconStyle ]} color={props.color} />
    )
  },
}, {
  name: "eventsTab",
  component: eventsTab,
  options:{ 
    tabBarLabel:"",
    // tabBarBadge: 3,
    // tabBarButton: (props: any) => (
    //   <View style={{ backgroundColor:'red', flex:1  }}>
    //     <BellTapped style={[ styles.tabIconStyle ]} color="#009900" />
    //   </View>
    // )
    tabBarIcon: (props: any) => (
      <BellTapped style={[ styles.tabIconStyle ]} color={props.color} />
    )
  },
}, {
  name: "profileTab",
  component: profileTab,
  options:{ 
    tabBarLabel:"",
    tabBarIcon: (props: any) => (
      <ProfileTaped style={[ styles.tabIconStyle ]} color={props.color} />
    )
  },
}];

const component = (props: any) => createTabs( initialRouteName, tabScreens );

export default component;
