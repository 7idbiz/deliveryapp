import React, { useEffect, } from 'react';
import * as Font from 'expo-font';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../core/store';
import {
  loadingFlagState,
  authFlagState,
} from '../core/userReducer';

import { 
  SplashScreen 
} from '../screens';
import Public from './Public';
import Secure from './Secure';

// modal loader for fullscren
// gwards

const loadFonts = (dispatch: any, loadingFlagState: any) => {
  let customFonts = {
    'SFDisplay-Black': require('../../assets/fonts/SFDisplay-Black.otf'),
    'SFDisplay-Bold': require('../../assets/fonts/SFDisplay-Bold.otf'),
    'SFDisplay-Heavy': require('../../assets/fonts/SFDisplay-Heavy.otf'),
    'SFDisplay-Light': require('../../assets/fonts/SFDisplay-Light.otf'),
    'SFDisplay-Medium': require('../../assets/fonts/SFDisplay-Medium.otf'),
    'SFDisplay-Regular': require('../../assets/fonts/SFDisplay-Regular.otf'),
    'SFDisplay-Semibold': require('../../assets/fonts/SFDisplay-Semibold.otf'),
    'SFDisplay-Thin': require('../../assets/fonts/SFDisplay-Thin.otf'),
    'SFDisplay-Ultralight': require('../../assets/fonts/SFDisplay-Ultralight.otf'),

    'SFText-Bold': require('../../assets/fonts/SFText-Bold.otf'),
    'SFText-BoldItalic': require('../../assets/fonts/SFText-BoldItalic.otf'),
    'SFText-Heavy': require('../../assets/fonts/SFText-Heavy.otf'),
    'SFText-HeavyItalic': require('../../assets/fonts/SFText-HeavyItalic.otf'),
    'SFText-Light': require('../../assets/fonts/SFText-Light.otf'),
    'SFText-LightItalic': require('../../assets/fonts/SFText-LightItalic.otf'),
    'SFText-Medium': require('../../assets/fonts/SFText-Medium.otf'),
    'SFText-MediumItalic': require('../../assets/fonts/SFText-MediumItalic.otf'),
    'SFText-Regular': require('../../assets/fonts/SFText-Regular.otf'),
    'SFText-RegularItalic': require('../../assets/fonts/SFText-RegularItalic.otf'),
    'SFText-Semibold': require('../../assets/fonts/SFText-Semibold.otf'),
    'SFText-SemiboldItalic': require('../../assets/fonts/SFText-SemiboldItalic.otf'),
  };

  async function loadFontsAsync() {
    await Font.loadAsync(customFonts);
    dispatch( loadingFlagState(true) );
  }
  loadFontsAsync();
};

const component = () => {
  const dispatch = useDispatch();
  const userState = useSelector((state: RootState) => state.user);

  useEffect(()=>{
    // dispatch( authFlagState(true) );
    loadFonts(dispatch, loadingFlagState);
    setTimeout(()=>{
      dispatch( loadingFlagState(true) );
    },800)
  }, []); 

  if (!userState.loadingFlag) {
    const Loader = SplashScreen;
    return <Loader />
  }

  const Layout = userState.authFlag
    ? Secure : Public;

  return <Layout />;
};

export default component;
