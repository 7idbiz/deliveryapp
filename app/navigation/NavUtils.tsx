import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Dimensions } from 'react-native';

import styles, { COLORS, DEVICE } from '../styles';

const createStack = (
  initialRouteName: string, 
  stackScreens: any
) => {
  const Stack = createStackNavigator();  

  const renderScreens = () => (
    stackScreens.map((props: any) => 
      <Stack.Screen {...props} key="{{props.name}}">
        {(propsScreen)=> <props.screen {...propsScreen}/>}
      </Stack.Screen>
    )
  )

  return (
    <Stack.Navigator
      initialRouteName={initialRouteName}>      
      { renderScreens() }
    </Stack.Navigator>
  );
};

const createTabs = ( 
  initialRouteName: string, 
  tabScreens: any 
) => {
  const Tab = createBottomTabNavigator();

  const renderTabs = () => (
    tabScreens.map((props: any) => <Tab.Screen {...props} key="{{props.name}}" />)
  );

  return(
    <Tab.Navigator 
      tabBarOptions={{
        activeTintColor: COLORS.white,
        inactiveTintColor: COLORS.grey,
        activeBackgroundColor: COLORS.black,
        inactiveBackgroundColor: COLORS.black,
        style: styles.tabPanelStyles,
      }}
      
      initialRouteName={initialRouteName} >
      { renderTabs() }
    </Tab.Navigator>
  )
}

export {
  createStack,
  createTabs,
}
