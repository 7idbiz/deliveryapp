import React from 'react';

import {
  createStack,
} from './NavUtils'

import {
  FontsScreen,
  CarrierConfirmationScreen,
  CarrierEnterPhoneScreen,
  CarrierRegistrationScreen,

  EnterPasswordScreen,
  EnterPhoneScreen,
  ForgotPasswordScreen,
  LoaderFullScreen,
  RegistrationScreen,
  SignInScreen,
  IconsScreen,
  TermsOfServiceScreen,
} from '../screens';

// const initialRouteName = "CarrierEnterPhoneScreen";
const initialRouteName = "EnterPhoneScreen";

const stackScreens = [{
  name: "EnterPhoneScreen",
  screen: EnterPhoneScreen, //
  options: {headerShown: false}
}, {
  name: "EnterPasswordScreen",
  screen: EnterPasswordScreen, //
  options: {headerShown: false}
}, {
  name: "SignInScreen",
  screen: SignInScreen, //
  options: {headerShown: false}
}, {
  name: "TermsOfServiceScreen",
  screen: TermsOfServiceScreen, //
  options: {headerShown: false}
}, {
  name: "ForgotPasswordScreen",
  screen: ForgotPasswordScreen, //
  options: {headerShown: false}
}, {
  name: "RegistrationScreen",
  screen: RegistrationScreen, //
  options: {headerShown: false}
}, {
  name: "LoaderFullScreen",
  screen: LoaderFullScreen, //
  options: {headerShown: false}
}, {
  name: "CarrierEnterPhoneScreen",
  screen: CarrierEnterPhoneScreen, //
  options: {headerShown: false}
}, {
  name: "CarrierConfirmationScreen",
  screen: CarrierConfirmationScreen, //
  options: {headerShown: false}
}, {
  name: "CarrierRegistrationScreen",
  screen: CarrierRegistrationScreen,
  options: {headerShown: false}
// }, {
//   name: "IconsScreen",
//   screen: IconsScreen,
//   options: {headerShown: false}
// }, {
//   name: "FontsScreen",
//   screen: FontsScreen,
//   options: {headerShown: false}
// }, {
//   name: "IconsScreen",
//   screen: IconsScreen,
//   options: {headerShown: false}
}];

const component = () => createStack( initialRouteName, stackScreens );

export default component;
